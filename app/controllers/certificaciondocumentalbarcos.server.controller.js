'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors'),
	CertificacionDocumentalBarco = mongoose.model('CertificacionDocumentalBarco'),
	_ = require('lodash');

/**
 * Create a CertificacionDocumentalBarco
 */
exports.create = function(req, res) {
	var certificaciondocumentalbarco = new CertificacionDocumentalBarco(req.body);
	certificaciondocumentalbarco.user = req.user;

	certificaciondocumentalbarco.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(certificaciondocumentalbarco);
		}
	});
};

/**
 * Show the current CertificacionDocumentalBarco
 */
exports.read = function(req, res) {

	res.jsonp(req.certificaciondocumentalbarco);
};

/**
 * Update a CertificacionDocumentalBarco
 */
exports.update = function(req, res) {
	var certificaciondocumentalbarco = req.certificaciondocumentalbarco ;

	certificaciondocumentalbarco = _.extend(certificaciondocumentalbarco , req.body);

	certificaciondocumentalbarco.updated = new Date();
	certificaciondocumentalbarco.updateUser = req.user;		

	certificaciondocumentalbarco.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(certificaciondocumentalbarco);
		}
	});
};

/**
 * Delete an CertificacionDocumentalBarco
 */
exports.delete = function(req, res) {
	var certificaciondocumentalbarco = req.certificaciondocumentalbarco ;

	certificaciondocumentalbarco.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(certificaciondocumentalbarco);
		}
	});
};

/**
 * List of CertificacionDocumentalBarcos
 */
exports.list = function(req, res) { CertificacionDocumentalBarco.find()
			.populate('barco')//.populate('barco.grupo')
			.sort('-created').populate('user', 'displayName')
			.exec(function(err, certificaciondocumentalbarcos) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(certificaciondocumentalbarcos);
		}
	});
};

/**
 * CertificacionDocumentalBarco middleware
 */
exports.certificaciondocumentalbarcoByID = function(req, res, next, id) { CertificacionDocumentalBarco.findById(id)
																				.populate('user', 'displayName')
																				.populate('updateUser', 'displayName')
																				.exec(function(err, certificaciondocumentalbarco) {
		if (err) return next(err);
		if (! certificaciondocumentalbarco) return next(new Error('Failed to load CertificacionDocumentalBarco ' + id));
		req.certificaciondocumentalbarco = certificaciondocumentalbarco ;
		next();
	});
};

/**
 * CertificacionDocumentalBarco authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (!req.certificaciondocumentalbarco.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};