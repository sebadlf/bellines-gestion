'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	moment = require('moment'),
	errorHandler = require('./errors'),
	Grupo = mongoose.model('Grupo'),
	Empresa = mongoose.model('Empresa'),
	CertificacionDocumentalEmpresa = mongoose.model('CertificacionDocumentalEmpresa'),
	CertificacionDocumentalBarco = mongoose.model('CertificacionDocumentalBarco'),
	CertificacionTecnicaBarco = mongoose.model('CertificacionTecnicaBarco'),
	_ = require('lodash');

/**
 * Create a Grupo
 */
exports.create = function(req, res) {
	var grupo = new Grupo(req.body);
	grupo.user = req.user;

	grupo.nombreLower = grupo.nombre.toLowerCase();

	grupo.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(grupo);
		}
	});
};

/**
 * Show the current Grupo
 */
exports.read = function(req, res) {

	res.jsonp(req.grupo);

};

/**
 * Update a Grupo
 */
exports.update = function(req, res) {
	var grupo = req.grupo;

	grupo = _.extend(grupo , req.body);

	grupo.nombreLower = grupo.nombre.toLowerCase();
	grupo.updated = new Date();
	grupo.updateUser = req.user;

	grupo.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(grupo);
		}
	});
};

/**
 * Delete an Grupo
 */
exports.delete = function(req, res) {
	var grupo = req.grupo ;

	grupo.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(grupo);
		}
	});
};

function populateCertificacionTecnicaBarco(req, res, result){
	var currentResult;

	function buscar (err, certificacionTecnicaBarcos) {

		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
								
			certificacionTecnicaBarcos.forEach(
				function(certificacionTecnicaBarco){

					if (certificacionTecnicaBarco.fechaDesde < new Date() && !certificacionTecnicaBarco.cumplida){

						result.push({
							type: 'certificaciontecnicabarcos',
							id: certificacionTecnicaBarco._id,
							desc: 'Certificación Tecnica Barco',
							fechaDesde: certificacionTecnicaBarco.fechaDesde, 
							fechaHasta: certificacionTecnicaBarco.fechaHasta, 
							nombre: certificacionTecnicaBarco.barco.nombre +
									' (' + certificacionTecnicaBarco.barco.grupo.nombre + ')'
						});

						currentResult = result[result.length - 1];
						currentResult.allData = currentResult.desc +
												currentResult.nombre +
												moment(currentResult.fechaDesde).format('DD-MM-YYYY') +
												moment(currentResult.fechaHasta).format('DD-MM-YYYY');
					}

					certificacionTecnicaBarco.verificaciones.forEach(function(verificacion){

						if (verificacion.fechaDesde < new Date() && !verificacion.cumplida){

							result.push({
								type: 'certificaciontecnicabarcos',
								id: certificacionTecnicaBarco._id,
								desc: verificacion.nombre,
								fechaDesde: verificacion.fechaDesde, 
								fechaHasta: verificacion.fechaHasta,
								nombre: certificacionTecnicaBarco.barco.nombre +
									' (' + certificacionTecnicaBarco.barco.grupo.nombre + ')'	
							});

							currentResult = result[result.length - 1];
							currentResult.allData = currentResult.desc +
													currentResult.nombre +
													moment(currentResult.fechaDesde).format('DD-MM-YYYY') +
													moment(currentResult.fechaHasta).format('DD-MM-YYYY');								

						}
					});

				}
			);

			result = _.sortBy(result,function(elem){
				return elem.fechaHasta;
			});
			
			res.jsonp(result);
		}
	}

	function PopulateGrupo(err, certificacionTecnicaBarcos) {
	      if(err) throw err;
	      // Deep population is here
	      Grupo.populate(certificacionTecnicaBarcos, { path: 'barco.grupo' }, buscar);
	}	

	CertificacionTecnicaBarco.find().populate('barco').exec(PopulateGrupo);		
}

function populateCertificacionDocumentalBarco(req, res, result){
	var currentResult;

	function buscar (err, certificacionDocumentalBarcos) {

		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
								
			certificacionDocumentalBarcos.forEach(
				function(certificacionDocumentalBarco){

					if (certificacionDocumentalBarco.fechaDesde < new Date() && !certificacionDocumentalBarco.cumplida){

						result.push({
							type: 'certificaciondocumentalbarcos',
							id: certificacionDocumentalBarco._id,
							desc: 'Certificación Documental Barco',
							fechaDesde: certificacionDocumentalBarco.fechaDesde, 
							fechaHasta: certificacionDocumentalBarco.fechaHasta, 
							nombre: certificacionDocumentalBarco.barco.nombre +
									' (' + certificacionDocumentalBarco.barco.grupo.nombre + ')'
						});

						currentResult = result[result.length - 1];
						currentResult.allData = currentResult.desc +
												currentResult.nombre +
												moment(currentResult.fechaDesde).format('DD-MM-YYYY') +
												moment(currentResult.fechaHasta).format('DD-MM-YYYY');							

					}

					certificacionDocumentalBarco.verificaciones.forEach(function(verificacion){

						if (verificacion.fechaDesde < new Date() && !verificacion.cumplida){

							result.push({
								type: 'certificaciondocumentalbarcos',
								id: certificacionDocumentalBarco._id,
								desc: verificacion.nombre,
								fechaDesde: verificacion.fechaDesde, 
								fechaHasta: verificacion.fechaHasta,
								nombre: certificacionDocumentalBarco.barco.nombre +
									' (' + certificacionDocumentalBarco.barco.grupo.nombre + ')'	
							});

							currentResult = result[result.length - 1];
							currentResult.allData = currentResult.desc +
													currentResult.nombre +
													moment(currentResult.fechaDesde).format('DD-MM-YYYY') +
													moment(currentResult.fechaHasta).format('DD-MM-YYYY');								

						}
					});

				}
			);
			
			populateCertificacionTecnicaBarco(req, res, result);
		}
	}

	function PopulateGrupo(err, certificacionDocumentalBarcos) {
	      if(err) throw err;
	      // Deep population is here
	      Grupo.populate(certificacionDocumentalBarcos, { path: 'barco.grupo' }, buscar);
	}	

	CertificacionDocumentalBarco.find().populate('barco').exec(PopulateGrupo);	
}

function populateCertificacionDocumentalEmpresa(req, res, result){
	var currentResult;

	function buscar (err, certificacionDocumentalEmpresas) {

		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
								
			certificacionDocumentalEmpresas.forEach(
				function(certificacionDocumentalEmpresa){

					if (certificacionDocumentalEmpresa.fechaDesde < new Date() && !certificacionDocumentalEmpresa.cumplida){

						result.push({
							type: 'certificaciondocumentalempresas',
							id: certificacionDocumentalEmpresa._id,
							desc: 'Certificación Documental Empresa',
							fechaDesde: certificacionDocumentalEmpresa.fechaDesde, 
							fechaHasta: certificacionDocumentalEmpresa.fechaHasta, 
							nombre: certificacionDocumentalEmpresa.empresa.nombre +
									' (' + certificacionDocumentalEmpresa.empresa.grupo.nombre + ')'
						});

						currentResult = result[result.length - 1];
						currentResult.allData = currentResult.desc +
												currentResult.nombre +
												moment(currentResult.fechaDesde).format('DD-MM-YYYY') +
												moment(currentResult.fechaHasta).format('DD-MM-YYYY');							

					}

					certificacionDocumentalEmpresa.verificaciones.forEach(function(verificacion){

						if (verificacion.fechaDesde < new Date() && !verificacion.cumplida){

							result.push({
								type: 'certificaciondocumentalempresas',
								id: certificacionDocumentalEmpresa._id,
								desc: verificacion.nombre,
								fechaDesde: verificacion.fechaDesde, 
								fechaHasta: verificacion.fechaHasta,
								nombre: certificacionDocumentalEmpresa.empresa.nombre +
									' (' + certificacionDocumentalEmpresa.empresa.grupo.nombre + ')'	
							});

							currentResult = result[result.length - 1];
							currentResult.allData = currentResult.desc +
													currentResult.nombre +
													moment(currentResult.fechaDesde).format('DD-MM-YYYY') +
													moment(currentResult.fechaHasta).format('DD-MM-YYYY');								

						}
					});

				}
			);
			
			populateCertificacionDocumentalBarco(req, res, result);
		}
	}	

	function PopulateGrupo(err, certificacionDocumentalEmpresas) {
	      if(err) throw err;
	      // Deep population is here
	      Grupo.populate(certificacionDocumentalEmpresas, { path: 'empresa.grupo' }, buscar);
	}

	CertificacionDocumentalEmpresa.find().populate('empresa').exec(PopulateGrupo);

}


/**
 * Dashboard
 */
exports.dashboard = function(req, res) { 

	var result = [];

	populateCertificacionDocumentalEmpresa(req, res, result);
};

/**
 * List of Grupos
 */
exports.list = function(req, res) { 

	Grupo.find().sort('nombreLower').populate('user', 'displayName').exec(
		function(err, grupos) {

			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(grupos);
			}
		}
	);
};


/**
 * Grupo middleware
 */
exports.grupoByID = function(req, res, next, id) { Grupo.findById(id)
													.populate('user', 'displayName')
													.populate('updateUser', 'displayName')
													.populate('empresas')
													.populate('barcos')
													.exec(function(err, grupo) {
		if (err) return next(err);
		if (! grupo) return next(new Error('Failed to load Grupo ' + id));
		req.grupo = grupo ;
		next();
	});
};

/**
 * Grupo authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {

	console.log(req.grupo.user.id);

	if (!req.grupo.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};