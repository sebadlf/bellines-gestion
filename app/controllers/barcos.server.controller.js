'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors'),
	Barco = mongoose.model('Barco'),
	Grupo = mongoose.model('Grupo'),
	_ = require('lodash');

/**
 * Create a Barco
 */
exports.create = function(req, res) {
	var barco = new Barco(req.body);
	barco.user = req.user;

	barco.nombreLower = barco.nombre.toLowerCase();

	barco.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(barco);
		}
	});
};

/**
 * Show the current Barco
 */
exports.read = function(req, res) {

	res.jsonp(req.barco);
	
};

/**
 * Update a Barco
 */
exports.update = function(req, res) {
	var barco = req.barco ;

	barco = _.extend(barco , req.body);

	barco.nombreLower = barco.nombre.toLowerCase();
	barco.updated = new Date();
	barco.updateUser = req.user;	

	barco.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(barco);
		}
	});
};

/**
 * Delete an Barco
 */
exports.delete = function(req, res) {
	var barco = req.barco ;

	barco.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(barco);
		}
	});
};

/**
 * List of Barcos
 */
exports.list = function(req, res) { Barco.find().sort('-created').populate('user', 'displayName').exec(function(err, barcos) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(barcos);
		}
	});
};

/**
 * Barco middleware
 */
exports.barcoByID = function(req, res, next, id) { Barco.findById(id)
														.populate('user', 'displayName')
														.populate('updateUser', 'displayName')
														.populate('grupo')
														.populate('certificacionesDocumentales')
														.populate('certificacionesTecnicas').exec(function(err, barco) {
		if (err) return next(err);
		if (! barco) return next(new Error('Failed to load Barco ' + id));
		req.barco = barco ;
		next();
	});
};

/**
 * Barco authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (!req.barco.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};