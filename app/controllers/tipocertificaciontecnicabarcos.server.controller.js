'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors'),
	Tipocertificaciontecnicabarco = mongoose.model('Tipocertificaciontecnicabarco'),
	_ = require('lodash');

/**
 * Create a Tipocertificaciontecnicabarco
 */
exports.create = function(req, res) {
	var tipocertificaciontecnicabarco = new Tipocertificaciontecnicabarco(req.body);
	tipocertificaciontecnicabarco.user = req.user;

	tipocertificaciontecnicabarco.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(tipocertificaciontecnicabarco);
		}
	});
};

/**
 * Show the current Tipocertificaciontecnicabarco
 */
exports.read = function(req, res) {
	res.jsonp(req.tipocertificaciontecnicabarco);
};

/**
 * Update a Tipocertificaciontecnicabarco
 */
exports.update = function(req, res) {
	var tipocertificaciontecnicabarco = req.tipocertificaciontecnicabarco ;

	tipocertificaciontecnicabarco = _.extend(tipocertificaciontecnicabarco , req.body);

	tipocertificaciontecnicabarco.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(tipocertificaciontecnicabarco);
		}
	});
};

/**
 * Delete an Tipocertificaciontecnicabarco
 */
exports.delete = function(req, res) {
	var tipocertificaciontecnicabarco = req.tipocertificaciontecnicabarco ;

	tipocertificaciontecnicabarco.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(tipocertificaciontecnicabarco);
		}
	});
};

/**
 * List of Tipocertificaciontecnicabarcos
 */
exports.list = function(req, res) { Tipocertificaciontecnicabarco.find().sort('-created').populate('user', 'displayName').exec(function(err, tipocertificaciontecnicabarcos) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(tipocertificaciontecnicabarcos);
		}
	});
};

/**
 * Tipocertificaciontecnicabarco middleware
 */
exports.tipocertificaciontecnicabarcoByID = function(req, res, next, id) { Tipocertificaciontecnicabarco.findById(id).populate('user', 'displayName').exec(function(err, tipocertificaciontecnicabarco) {
		if (err) return next(err);
		if (! tipocertificaciontecnicabarco) return next(new Error('Failed to load Tipocertificaciontecnicabarco ' + id));
		req.tipocertificaciontecnicabarco = tipocertificaciontecnicabarco ;
		next();
	});
};

/**
 * Tipocertificaciontecnicabarco authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (!req.tipocertificaciontecnicabarco.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};