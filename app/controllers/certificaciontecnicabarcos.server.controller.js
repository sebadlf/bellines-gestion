'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors'),
	CertificacionTecnicaBarco = mongoose.model('CertificacionTecnicaBarco'),
	_ = require('lodash');

/**
 * Create a CertificacionTecnicaBarco
 */
exports.create = function(req, res) {
	var certificaciontecnicabarco = new CertificacionTecnicaBarco(req.body);
	certificaciontecnicabarco.user = req.user;

	certificaciontecnicabarco.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(certificaciontecnicabarco);
		}
	});
};

/**
 * Show the current CertificacionTecnicaBarco
 */
exports.read = function(req, res) {

	res.jsonp(req.certificaciontecnicabarco);
};

/**
 * Update a CertificacionTecnicaBarco
 */
exports.update = function(req, res) {
	var certificaciontecnicabarco = req.certificaciontecnicabarco ;

	certificaciontecnicabarco = _.extend(certificaciontecnicabarco , req.body);

	certificaciontecnicabarco.updated = new Date();
	certificaciontecnicabarco.updateUser = req.user;		

	certificaciontecnicabarco.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(certificaciontecnicabarco);
		}
	});
};

/**
 * Delete an CertificacionTecnicaBarco
 */
exports.delete = function(req, res) {
	var certificaciontecnicabarco = req.certificaciontecnicabarco ;

	certificaciontecnicabarco.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(certificaciontecnicabarco);
		}
	});
};

/**
 * List of CertificacionTecnicaBarcos
 */
exports.list = function(req, res) { CertificacionTecnicaBarco.find()
			.populate('barco')//.populate('barco.grupo')
			.sort('-created').populate('user', 'displayName')
			.exec(function(err, certificaciontecnicabarcos) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(certificaciontecnicabarcos);
		}
	});
};

/**
 * CertificacionTecnicaBarco middleware
 */
exports.certificaciontecnicabarcoByID = function(req, res, next, id) { CertificacionTecnicaBarco.findById(id)
																				.populate('user', 'displayName')
																				.populate('updateUser', 'displayName')
																				.exec(function(err, certificaciontecnicabarco) {
		if (err) return next(err);
		if (! certificaciontecnicabarco) return next(new Error('Failed to load CertificacionTecnicaBarco ' + id));
		req.certificaciontecnicabarco = certificaciontecnicabarco ;
		next();
	});
};

exports.getVerificacionBarcoNombres = function (req, res){
	var result = [];

	CertificacionTecnicaBarco.find().lean().exec(
		function(err, certificacionTecnicaBarcos) {

			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
									
				certificacionTecnicaBarcos.forEach(
					function(certificacionTecnicaBarco){

						certificacionTecnicaBarco.verificaciones.forEach(function(verificacion){

							result.push({
								nombre: certificacionTecnicaBarco.barco.nombre	
							});

						});

					}
				);

				result = _.sortBy(result,function(elem){
					return elem.nombre;
				});
				
				res.jsonp(result);
			}
		}
	);	
};

/**
 * CertificacionTecnicaBarco authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (!req.certificaciontecnicabarco.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};