'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors'),
	CertificacionDocumentalEmpresa = mongoose.model('CertificacionDocumentalEmpresa'),
	_ = require('lodash');

/**
 * Create a CertificacionDocumentalEmpresa
 */
exports.create = function(req, res) {
	var certificaciondocumentalempresa = new CertificacionDocumentalEmpresa(req.body);
	certificaciondocumentalempresa.user = req.user;

	certificaciondocumentalempresa.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(certificaciondocumentalempresa);
		}
	});
};

/**
 * Show the current CertificacionDocumentalEmpresa
 */
exports.read = function(req, res) {

	res.jsonp(req.certificaciondocumentalempresa);
};

/**
 * Update a CertificacionDocumentalEmpresa
 */
exports.update = function(req, res) {
	var certificaciondocumentalempresa = req.certificaciondocumentalempresa ;

	certificaciondocumentalempresa = _.extend(certificaciondocumentalempresa , req.body);

	certificaciondocumentalempresa.updated = new Date();
	certificaciondocumentalempresa.updateUser = req.user;		

	certificaciondocumentalempresa.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(certificaciondocumentalempresa);
		}
	});
};

/**
 * Delete an CertificacionDocumentalEmpresa
 */
exports.delete = function(req, res) {
	var certificaciondocumentalempresa = req.certificaciondocumentalempresa ;

	certificaciondocumentalempresa.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(certificaciondocumentalempresa);
		}
	});
};

/**
 * List of CertificacionDocumentalEmpresas
 */
exports.list = function(req, res) { CertificacionDocumentalEmpresa.find()
			.populate('empresa')//.populate('empresa.grupo')
			.sort('-created').populate('user', 'displayName')
			.exec(function(err, certificaciondocumentalempresas) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(certificaciondocumentalempresas);
		}
	});
};

/**
 * CertificacionDocumentalEmpresa middleware
 */
exports.certificaciondocumentalempresaByID = function(req, res, next, id) { CertificacionDocumentalEmpresa.findById(id)
																				.populate('user', 'displayName')
																				.populate('updateUser', 'displayName')
																				.exec(function(err, certificaciondocumentalempresa) {
		if (err) return next(err);
		if (! certificaciondocumentalempresa) return next(new Error('Failed to load CertificacionDocumentalEmpresa ' + id));
		req.certificaciondocumentalempresa = certificaciondocumentalempresa ;
		next();
	});
};

/**
 * CertificacionDocumentalEmpresa authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (!req.certificaciondocumentalempresa.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};