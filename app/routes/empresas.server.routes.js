'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var empresas = require('../../app/controllers/empresas');

	// Empresas Routes
	app.route('/empresas')
		.get(users.requiresLogin, empresas.list)
		.post(users.requiresLogin, empresas.create);

	app.route('/empresas/:empresaId')
		.get(users.requiresLogin, empresas.read)
		.put(users.requiresLogin, empresas.hasAuthorization, empresas.update)
		.delete(users.requiresLogin, empresas.hasAuthorization, empresas.delete);

	// Finish by binding the Empresa middleware
	app.param('empresaId', empresas.empresaByID);
};