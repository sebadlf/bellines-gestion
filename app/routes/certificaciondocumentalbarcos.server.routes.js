'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var certificaciondocumentalbarcos = require('../../app/controllers/certificaciondocumentalbarcos');

	// CertificacionDocumentalBarcos Routes
	app.route('/certificaciondocumentalbarcos')
		.get(users.requiresLogin, certificaciondocumentalbarcos.list)
		.post(users.requiresLogin, certificaciondocumentalbarcos.create);

	app.route('/certificaciondocumentalbarcos/:certificaciondocumentalbarcoId')
		.get(users.requiresLogin, certificaciondocumentalbarcos.read)
		.put(users.requiresLogin, certificaciondocumentalbarcos.hasAuthorization, certificaciondocumentalbarcos.update)
		.delete(users.requiresLogin, certificaciondocumentalbarcos.hasAuthorization, certificaciondocumentalbarcos.delete);

	// Finish by binding the CertificacionDocumentalBarco middleware
	app.param('certificaciondocumentalbarcoId', certificaciondocumentalbarcos.certificaciondocumentalbarcoByID);
};