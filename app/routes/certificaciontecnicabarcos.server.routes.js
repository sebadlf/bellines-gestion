'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var certificaciontecnicabarcos = require('../../app/controllers/certificaciontecnicabarcos');

	// CertificacionTecnicaBarcos Routes
	app.route('/certificaciontecnicabarcos')
		.get(users.requiresLogin, certificaciontecnicabarcos.list)
		.post(users.requiresLogin, certificaciontecnicabarcos.create);

	app.route('/certificaciontecnicabarcos/:certificaciontecnicabarcoId')
		.get(users.requiresLogin, certificaciontecnicabarcos.read)
		.put(users.requiresLogin, certificaciontecnicabarcos.hasAuthorization, certificaciontecnicabarcos.update)
		.delete(users.requiresLogin, certificaciontecnicabarcos.hasAuthorization, certificaciontecnicabarcos.delete);

	// Finish by binding the CertificacionTecnicaBarco middleware
	app.param('certificaciontecnicabarcoId', certificaciontecnicabarcos.certificaciontecnicabarcoByID);
};