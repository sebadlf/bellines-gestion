'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var tipocertificaciontecnicabarcos = require('../../app/controllers/tipocertificaciontecnicabarcos');

	// Tipocertificaciontecnicabarcos Routes
	app.route('/tipocertificaciontecnicabarcos')
		.get(users.requiresLogin, tipocertificaciontecnicabarcos.list)
		.post(users.requiresLogin, tipocertificaciontecnicabarcos.create);

	app.route('/tipocertificaciontecnicabarcos/:tipocertificaciontecnicabarcoId')
		.get(users.requiresLogin, tipocertificaciontecnicabarcos.read)
		.put(users.requiresLogin, tipocertificaciontecnicabarcos.hasAuthorization, tipocertificaciontecnicabarcos.update)
		.delete(users.requiresLogin, tipocertificaciontecnicabarcos.hasAuthorization, tipocertificaciontecnicabarcos.delete);

	// Finish by binding the Tipocertificaciontecnicabarco middleware
	app.param('tipocertificaciontecnicabarcoId', tipocertificaciontecnicabarcos.tipocertificaciontecnicabarcoByID);
};