'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var certificaciondocumentalempresas = require('../../app/controllers/certificaciondocumentalempresas');

	// CertificacionDocumentalEmpresas Routes
	app.route('/certificaciondocumentalempresas')
		.get(users.requiresLogin, certificaciondocumentalempresas.list)
		.post(users.requiresLogin, certificaciondocumentalempresas.create);

	app.route('/certificaciondocumentalempresas/:certificaciondocumentalempresaId')
		.get(users.requiresLogin, certificaciondocumentalempresas.read)
		.put(users.requiresLogin, certificaciondocumentalempresas.hasAuthorization, certificaciondocumentalempresas.update)
		.delete(users.requiresLogin, certificaciondocumentalempresas.hasAuthorization, certificaciondocumentalempresas.delete);

	// Finish by binding the CertificacionDocumentalEmpresa middleware
	app.param('certificaciondocumentalempresaId', certificaciondocumentalempresas.certificaciondocumentalempresaByID);
};