'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var barcos = require('../../app/controllers/barcos');

	// Barcos Routes
	app.route('/barcos')
		.get(users.requiresLogin, barcos.list)
		.post(users.requiresLogin, barcos.create);

	app.route('/barcos/:barcoId')
		.get(users.requiresLogin, barcos.read)
		.put(users.requiresLogin, barcos.hasAuthorization, barcos.update)
		.delete(users.requiresLogin, barcos.hasAuthorization, barcos.delete);

	// Finish by binding the Barco middleware
	app.param('barcoId', barcos.barcoByID);
};