'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
    relationship = require('mongoose-relationship');

/**
 * Barco Schema
 */
var BarcoSchema = new Schema({
	nombre: {
		type: String,
		default: '',
		required: 'Nombre es un campo requerido',
		trim: true
	},
	nombreLower: {
		type: String,
		trim: true
	},	
	grupo: {
		type: Schema.ObjectId,
		ref: 'Grupo',
		childPath: 'barcos'
	},
	certificacionesDocumentales: [{
		type: Schema.ObjectId,
		ref: 'CertificacionDocumentalBarco'
	}],	
	certificacionesTecnicas: [{
		type: Schema.ObjectId,
		ref: 'CertificacionTecnicaBarco'
	}],		
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	updated: {
		type: Date
	},
	updateUser: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

BarcoSchema.plugin(relationship, { relationshipPathName:'grupo' });
mongoose.model('Barco', BarcoSchema);