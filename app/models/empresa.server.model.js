'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
    relationship = require('mongoose-relationship');

/**
 * Empresa Schema
 */
var EmpresaSchema = new Schema({
	nombre: {
		type: String,
		default: '',
		required: 'Nombre es un campo requerido',
		trim: true
	},
	nombreLower: {
		type: String,
		trim: true
	},	
	grupo: {
		type: Schema.ObjectId,
		ref: 'Grupo',
		childPath: 'empresas'
	},
	certificacionesDocumentales: [{
		type: Schema.ObjectId,
		ref: 'CertificacionDocumentalEmpresa'
	}],	
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	updated: {
		type: Date
	},
	updateUser: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

EmpresaSchema.plugin(relationship, { relationshipPathName:'grupo' });
mongoose.model('Empresa', EmpresaSchema);