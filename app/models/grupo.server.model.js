'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Grupo Schema
 */
var GrupoSchema = new Schema({
	nombre: {
		type: String,
		default: '',
		required: 'Nombre es un campo requerido',
		trim: true
	},
	nombreLower: {
		type: String,
		trim: true
	},
	empresas: [{
		type: Schema.ObjectId,
		ref: 'Empresa'
	}],
	barcos: [{
		type: Schema.ObjectId,
		ref: 'Barco'
	}],	
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	updated: {
		type: Date
	},
	updateUser: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Grupo', GrupoSchema);