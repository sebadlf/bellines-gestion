'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	relationship = require('mongoose-relationship');


var VerificacionSchema = new Schema({
	nombre: {
		type: String,
		default: ''
	},
	anioVerificacion: {
		type: Number
	},
	fechaDesde: {
		type: Date,
		default: Date.now
	},
	fechaHasta: {
		type: Date,
		default: Date.now
	},	
	cumplida: {
		type: Boolean,
		default: '',
	},
	comentarios: {
		type: String,
		default: '',
	}
});

/**
 * CertificacionTecnicaBarco Schema
 */
var CertificacionTecnicaBarcoSchema = new Schema({
	barco: {
		type: Schema.ObjectId,
		ref: 'Barco',
		childPath: 'certificacionesTecnicas'
	},	
	fechaDesde: {
		type: Date,
		default: Date.now
	},
	fechaHasta: {
		type: Date,
		default: Date.now
	},	
	cumplida: {
		type: Boolean,
		default: '',
	},
	comentarios: {
		type: String,
		default: '',
	},	
	verificaciones: [VerificacionSchema],
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	updated: {
		type: Date
	},
	updateUser: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

CertificacionTecnicaBarcoSchema.plugin(relationship, { relationshipPathName:'barco' });
mongoose.model('CertificacionTecnicaBarco', CertificacionTecnicaBarcoSchema);