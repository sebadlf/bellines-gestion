'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	relationship = require('mongoose-relationship');


var VerificacionSchema = new Schema({
	nombre: {
		type: String,
		default: ''
	},
	anioVerificacion: {
		type: Number
	},
	fechaDesde: {
		type: Date,
		default: Date.now
	},
	fechaHasta: {
		type: Date,
		default: Date.now
	},	
	cumplida: {
		type: Boolean,
		default: '',
	},
	comentarios: {
		type: String,
		default: '',
	}
});

/**
 * CertificacionDocumentalEmpresa Schema
 */
var CertificacionDocumentalEmpresaSchema = new Schema({
	empresa: {
		type: Schema.ObjectId,
		ref: 'Empresa',
		childPath: 'certificacionesDocumentales'
	},	
	fechaDesde: {
		type: Date,
		default: Date.now
	},
	fechaHasta: {
		type: Date,
		default: Date.now
	},	
	cumplida: {
		type: Boolean,
		default: '',
	},
	comentarios: {
		type: String,
		default: '',
	},	
	verificaciones: [VerificacionSchema],
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	updated: {
		type: Date
	},
	updateUser: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

CertificacionDocumentalEmpresaSchema.plugin(relationship, { relationshipPathName:'empresa' });
mongoose.model('CertificacionDocumentalEmpresa', CertificacionDocumentalEmpresaSchema);