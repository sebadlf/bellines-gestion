'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Tipocertificaciontecnicabarco Schema
 */
var TipocertificaciontecnicabarcoSchema = new Schema({
	nombre: {
		type: String,
		default: '',
		required: 'Nombre es un campo requerido',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Tipocertificaciontecnicabarco', TipocertificaciontecnicabarcoSchema);