'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	CertificacionDocumentalEmpresa = mongoose.model('CertificacionDocumentalEmpresa');

/**
 * Globals
 */
var user, certificaciondocumentalempresa;

/**
 * Unit tests
 */
describe('CertificacionDocumentalEmpresa Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			certificaciondocumentalempresa = new CertificacionDocumentalEmpresa({
				name: 'CertificacionDocumentalEmpresa Name',
				user: user
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return certificaciondocumentalempresa.save(function(err) {
				should.not.exist(err);
				done();
			});
		});

		it('should be able to show an error when try to save without name', function(done) { 
			certificaciondocumentalempresa.name = '';

			return certificaciondocumentalempresa.save(function(err) {
				should.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		CertificacionDocumentalEmpresa.remove().exec();
		User.remove().exec();

		done();
	});
});