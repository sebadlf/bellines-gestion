'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Certificaciontecnicabarco = mongoose.model('Certificaciontecnicabarco');

/**
 * Globals
 */
var user, certificaciontecnicabarco;

/**
 * Unit tests
 */
describe('Certificaciontecnicabarco Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			certificaciontecnicabarco = new Certificaciontecnicabarco({
				name: 'Certificaciontecnicabarco Name',
				user: user
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return certificaciontecnicabarco.save(function(err) {
				should.not.exist(err);
				done();
			});
		});

		it('should be able to show an error when try to save without name', function(done) { 
			certificaciontecnicabarco.name = '';

			return certificaciontecnicabarco.save(function(err) {
				should.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		Certificaciontecnicabarco.remove().exec();
		User.remove().exec();

		done();
	});
});