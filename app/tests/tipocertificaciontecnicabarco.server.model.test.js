'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Tipocertificaciontecnicabarco = mongoose.model('Tipocertificaciontecnicabarco');

/**
 * Globals
 */
var user, tipocertificaciontecnicabarco;

/**
 * Unit tests
 */
describe('Tipocertificaciontecnicabarco Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			tipocertificaciontecnicabarco = new Tipocertificaciontecnicabarco({
				name: 'Tipocertificaciontecnicabarco Name',
				user: user
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return tipocertificaciontecnicabarco.save(function(err) {
				should.not.exist(err);
				done();
			});
		});

		it('should be able to show an error when try to save without name', function(done) { 
			tipocertificaciontecnicabarco.name = '';

			return tipocertificaciontecnicabarco.save(function(err) {
				should.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		Tipocertificaciontecnicabarco.remove().exec();
		User.remove().exec();

		done();
	});
});