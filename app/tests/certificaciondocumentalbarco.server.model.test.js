'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Certificaciondocumentalbarco = mongoose.model('Certificaciondocumentalbarco');

/**
 * Globals
 */
var user, certificaciondocumentalbarco;

/**
 * Unit tests
 */
describe('Certificaciondocumentalbarco Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			certificaciondocumentalbarco = new Certificaciondocumentalbarco({
				name: 'Certificaciondocumentalbarco Name',
				user: user
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return certificaciondocumentalbarco.save(function(err) {
				should.not.exist(err);
				done();
			});
		});

		it('should be able to show an error when try to save without name', function(done) { 
			certificaciondocumentalbarco.name = '';

			return certificaciondocumentalbarco.save(function(err) {
				should.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		Certificaciondocumentalbarco.remove().exec();
		User.remove().exec();

		done();
	});
});