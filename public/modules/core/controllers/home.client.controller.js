'use strict';

angular.module('core').controller('HomeController', ['$scope', 'Authentication', '$location',
	function($scope, Authentication, $location) {
		// This provides Authentication context.
		$scope.authentication = Authentication;

		$scope.initScreen = function(){
			if ($scope.authentication.user){
				$location.path('/dashboard');
			} else {
				$location.path('/signin');
			}
		};
	}
]);