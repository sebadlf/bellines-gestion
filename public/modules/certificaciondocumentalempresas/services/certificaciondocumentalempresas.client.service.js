'use strict';

//CertificacionDocumentalEmpresas service used to communicate CertificacionDocumentalEmpresas REST endpoints
angular.module('certificaciondocumentalempresas').factory('CertificacionDocumentalEmpresas', ['$resource',
	function($resource) {
		return $resource('certificaciondocumentalempresas/:certificaciondocumentalempresaId', { certificaciondocumentalempresaId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);