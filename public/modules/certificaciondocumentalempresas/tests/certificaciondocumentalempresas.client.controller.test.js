'use strict';

(function() {
	// CertificacionDocumentalEmpresas Controller Spec
	describe('CertificacionDocumentalEmpresas Controller Tests', function() {
		// Initialize global variables
		var CertificacionDocumentalEmpresasController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the CertificacionDocumentalEmpresas controller.
			CertificacionDocumentalEmpresasController = $controller('CertificacionDocumentalEmpresasController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one CertificacionDocumentalEmpresa object fetched from XHR', inject(function(CertificacionDocumentalEmpresas) {
			// Create sample CertificacionDocumentalEmpresa using the CertificacionDocumentalEmpresas service
			var sampleCertificacionDocumentalEmpresa = new CertificacionDocumentalEmpresas({
				name: 'New CertificacionDocumentalEmpresa'
			});

			// Create a sample CertificacionDocumentalEmpresas array that includes the new CertificacionDocumentalEmpresa
			var sampleCertificacionDocumentalEmpresas = [sampleCertificacionDocumentalEmpresa];

			// Set GET response
			$httpBackend.expectGET('certificaciondocumentalempresas').respond(sampleCertificacionDocumentalEmpresas);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.certificaciondocumentalempresas).toEqualData(sampleCertificacionDocumentalEmpresas);
		}));

		it('$scope.findOne() should create an array with one CertificacionDocumentalEmpresa object fetched from XHR using a certificaciondocumentalempresaId URL parameter', inject(function(CertificacionDocumentalEmpresas) {
			// Define a sample CertificacionDocumentalEmpresa object
			var sampleCertificacionDocumentalEmpresa = new CertificacionDocumentalEmpresas({
				name: 'New CertificacionDocumentalEmpresa'
			});

			// Set the URL parameter
			$stateParams.certificaciondocumentalempresaId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/certificaciondocumentalempresas\/([0-9a-fA-F]{24})$/).respond(sampleCertificacionDocumentalEmpresa);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.certificaciondocumentalempresa).toEqualData(sampleCertificacionDocumentalEmpresa);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(CertificacionDocumentalEmpresas) {
			// Create a sample CertificacionDocumentalEmpresa object
			var sampleCertificacionDocumentalEmpresaPostData = new CertificacionDocumentalEmpresas({
				name: 'New CertificacionDocumentalEmpresa'
			});

			// Create a sample CertificacionDocumentalEmpresa response
			var sampleCertificacionDocumentalEmpresaResponse = new CertificacionDocumentalEmpresas({
				_id: '525cf20451979dea2c000001',
				name: 'New CertificacionDocumentalEmpresa'
			});

			// Fixture mock form input values
			scope.name = 'New CertificacionDocumentalEmpresa';

			// Set POST response
			$httpBackend.expectPOST('certificaciondocumentalempresas', sampleCertificacionDocumentalEmpresaPostData).respond(sampleCertificacionDocumentalEmpresaResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the CertificacionDocumentalEmpresa was created
			expect($location.path()).toBe('/certificaciondocumentalempresas/' + sampleCertificacionDocumentalEmpresaResponse._id);
		}));

		it('$scope.update() should update a valid CertificacionDocumentalEmpresa', inject(function(CertificacionDocumentalEmpresas) {
			// Define a sample CertificacionDocumentalEmpresa put data
			var sampleCertificacionDocumentalEmpresaPutData = new CertificacionDocumentalEmpresas({
				_id: '525cf20451979dea2c000001',
				name: 'New CertificacionDocumentalEmpresa'
			});

			// Mock CertificacionDocumentalEmpresa in scope
			scope.certificaciondocumentalempresa = sampleCertificacionDocumentalEmpresaPutData;

			// Set PUT response
			$httpBackend.expectPUT(/certificaciondocumentalempresas\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/certificaciondocumentalempresas/' + sampleCertificacionDocumentalEmpresaPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid certificaciondocumentalempresaId and remove the CertificacionDocumentalEmpresa from the scope', inject(function(CertificacionDocumentalEmpresas) {
			// Create new CertificacionDocumentalEmpresa object
			var sampleCertificacionDocumentalEmpresa = new CertificacionDocumentalEmpresas({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new CertificacionDocumentalEmpresas array and include the CertificacionDocumentalEmpresa
			scope.certificaciondocumentalempresas = [sampleCertificacionDocumentalEmpresa];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/certificaciondocumentalempresas\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleCertificacionDocumentalEmpresa);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.certificaciondocumentalempresas.length).toBe(0);
		}));
	});
}());