'use strict';

//Setting up route
angular.module('certificaciondocumentalempresas').config(['$stateProvider',
	function($stateProvider) {
		// CertificacionDocumentalEmpresas state routing
		$stateProvider.
		state('listCertificacionDocumentalEmpresas', {
			url: '/certificaciondocumentalempresas',
			templateUrl: 'modules/certificaciondocumentalempresas/views/list-certificaciondocumentalempresas.client.view.html'
		}).
		state('createCertificacionDocumentalEmpresa', {
			url: '/certificaciondocumentalempresas/create/:empresaId',
			templateUrl: 'modules/certificaciondocumentalempresas/views/create-certificaciondocumentalempresa.client.view.html'
		}).
		state('viewCertificacionDocumentalEmpresa', {
			url: '/certificaciondocumentalempresas/:certificaciondocumentalempresaId',
			templateUrl: 'modules/certificaciondocumentalempresas/views/view-certificaciondocumentalempresa.client.view.html'
		}).
		state('editCertificacionDocumentalEmpresa', {
			url: '/certificaciondocumentalempresas/:certificaciondocumentalempresaId/edit',
			templateUrl: 'modules/certificaciondocumentalempresas/views/edit-certificaciondocumentalempresa.client.view.html'
		});
	}
]);