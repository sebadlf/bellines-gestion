'use strict';

// CertificacionDocumentalEmpresas controller
angular.module('certificaciondocumentalempresas').controller('CertificacionDocumentalEmpresasController', ['$scope', '$stateParams', '$location', 'Authentication', 'CertificacionDocumentalEmpresas', 'Grupos', 'Empresas', '$filter', 'Menus', 'moment', 'lodash',
    function($scope, $stateParams, $location, Authentication, CertificacionDocumentalEmpresas, Grupos, Empresas, $filter, Menus, moment, lodash) {
        $scope.authentication = Authentication;

        $scope.opened = {};

        $scope.empresa = {};

        $scope.certificaciondocumentalempresa = {};

        $scope.isMenuGenerated = false;

        $scope.isNew = $stateParams.empresaId;

        var _ = lodash;


        var getEmpresaInfo = function(empresaId) {
            $scope.empresa = Empresas.get({
                    empresaId: empresaId
                })
                .$promise.then(function(empresa) {
                    $scope.empresa = empresa;

                    if (!$scope.isMenuGenerated){

                        Menus.addMenuItem('topbar', 'Grupo', 'grupos/' + empresa.grupo._id);
                        Menus.addMenuItem('topbar', 'Empresa', 'empresas/' + $scope.empresa._id);

                        $scope.isMenuGenerated = true;
                    }
                });
        };

        $scope.generarFechas = function() {

            var fechaDesde = $scope.certificaciondocumentalempresa.fechaDesde;

            console.log('generarFechas', fechaDesde);

            $scope.certificaciondocumentalempresa.fechaHasta = moment(fechaDesde).add(5, 'year').toDate();

            for (var i = 0; i <= $scope.certificaciondocumentalempresa.verificaciones.length - 1; i++) {
                var verificacion = $scope.certificaciondocumentalempresa.verificaciones[i];
                verificacion.fechaDesde = moment(fechaDesde).add(i + 1, 'year').add(-3, 'month').toDate();
                verificacion.fechaHasta = moment(fechaDesde).add(i + 1, 'year').add(+3, 'month').toDate();
            }
        };

        $scope.habilitarBotones = function(){

            function habilitarCumplidaFunction(verificacion) {

                return !_.find($scope.certificaciondocumentalempresa.verificaciones, function(otraVerificacion){
                    return otraVerificacion.anioVerificacion < verificacion.anioVerificacion && !otraVerificacion.cumplida;
                });

            }

            $scope.certificaciondocumentalempresa.habilitarCumplida = 
            !_.find($scope.certificaciondocumentalempresa.verificaciones, function(verificacion){
                return !verificacion.cumplida;
            });

            if (!$scope.certificaciondocumentalempresa.habilitarCumplida){
                $scope.certificaciondocumentalempresa.cumplida = false;
            }

            for (var i = 0; i < $scope.certificaciondocumentalempresa.verificaciones.length; i++) {
                var verificacion = $scope.certificaciondocumentalempresa.verificaciones[i];

                verificacion.habilitarCumplida = habilitarCumplidaFunction(verificacion);

                if (!verificacion.habilitarCumplida){
                    verificacion.cumplida = false;
                }
            }
        };

        if ($stateParams.empresaId) {
            getEmpresaInfo($stateParams.empresaId);

            $scope.certificaciondocumentalempresa.verificaciones = [];

            for (var i = 1; i <= 4; i++) {
                $scope.certificaciondocumentalempresa.verificaciones.push({
                    nombre: 'Verificación Anual ' + i,
                    anioVerificacion: i
                });
            }

            $scope.certificaciondocumentalempresa.fechaDesde = new Date();

            $scope.generarFechas();

            $scope.habilitarBotones();
        }

        // Create new CertificacionDocumentalEmpresa
        $scope.create = function() {
            // Create new CertificacionDocumentalEmpresa object
            var certificaciondocumentalempresa = new CertificacionDocumentalEmpresas({
                empresa: $stateParams.empresaId,
                fechaDesde: $scope.certificaciondocumentalempresa.fechaDesde,
                fechaHasta: $scope.certificaciondocumentalempresa.fechaHasta,
                cumplida: $scope.certificaciondocumentalempresa.cumplida,
                comentarios: $scope.certificaciondocumentalempresa.comentarios,
                verificaciones: $scope.certificaciondocumentalempresa.verificaciones
            });

            // Redirect after save
            certificaciondocumentalempresa.$save(function(response) {
                $location.path('empresas/' + $scope.empresa._id);

                // Clear form fields
                $scope.fechaDesde = null;
                $scope.fechaHasta = null;
                $scope.cumplida = false;
                $scope.comentarios = '';
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing CertificacionDocumentalEmpresa
        $scope.remove = function(certificaciondocumentalempresa) {
            $scope.certificaciondocumentalempresa.$remove(function() {
                $location.path('empresas/' + $scope.empresa._id);
            });
        };

        // Update existing CertificacionDocumentalEmpresa
        $scope.update = function() {
            var certificaciondocumentalempresa = $scope.certificaciondocumentalempresa;

            certificaciondocumentalempresa.$update(function() {

                    $location.path('empresas/' + $scope.empresa._id);
                
                    /*
                    $scope.certificaciondocumentalempresa.fechaDesde = new Date($scope.certificaciondocumentalempresa.fechaDesde);
                    $scope.certificaciondocumentalempresa.fechaHasta = new Date($scope.certificaciondocumentalempresa.fechaHasta);

                    for (var i = 0; i < certificaciondocumentalempresa.verificaciones.length; i++) {
                        var verificacion = certificaciondocumentalempresa.verificaciones[i];

                        verificacion.fechaDesde = new Date(verificacion.fechaDesde);
                        verificacion.fechaHasta = new Date(verificacion.fechaHasta);
                    }

                    $scope.habilitarBotones();

                    getEmpresaInfo(certificaciondocumentalempresa.empresa);                

                    */                  
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of CertificacionDocumentalEmpresas
        $scope.find = function() {
            $scope.certificaciondocumentalempresas = CertificacionDocumentalEmpresas.query();
        };

        // Find existing CertificacionDocumentalEmpresa
        $scope.findOne = function() {
            CertificacionDocumentalEmpresas.get({
                    certificaciondocumentalempresaId: $stateParams.certificaciondocumentalempresaId
                })
                .$promise.then(function(certificaciondocumentalempresa) {
                    $scope.certificaciondocumentalempresa = certificaciondocumentalempresa;

                    $scope.certificaciondocumentalempresa.fechaDesde = new Date($scope.certificaciondocumentalempresa.fechaDesde);
                    $scope.certificaciondocumentalempresa.fechaHasta = new Date($scope.certificaciondocumentalempresa.fechaHasta);

                    for (var i = 0; i < certificaciondocumentalempresa.verificaciones.length; i++) {
                        var verificacion = certificaciondocumentalempresa.verificaciones[i];

                        verificacion.fechaDesde = new Date(verificacion.fechaDesde);
                        verificacion.fechaHasta = new Date(verificacion.fechaHasta);
                    }

                    $scope.habilitarBotones();

                    getEmpresaInfo(certificaciondocumentalempresa.empresa);
                });
        };

        $scope.nuevaVerificacionComplementaria = function() {
            $scope.certificaciondocumentalempresa.verificaciones.push({
                nombre: 'Verificación Adicional',
                fechaDesde: new Date(),
                fechaHasta: moment(new Date()).add(3, 'month').toDate()
            });

            $scope.habilitarBotones();
        };

        $scope.eliminarVerificacionAnual = function(verificacionAnual) {
            verificacionAnual.deleted = true;

            console.log($scope.certificaciondocumentalempresa.verificaciones);

            $scope.certificaciondocumentalempresa.verificaciones =
            _.filter($scope.certificaciondocumentalempresa.verificaciones, function(verificacion){
                return !verificacion.deleted;
            });
        };

        $scope.open = function($event, control, index) {
            $event.preventDefault();
            $event.stopPropagation();

            console.log(control, index);

            if (index === undefined) {
                $scope.opened[control] = true;
            } else {
                if (!$scope.opened[control]) {
                    $scope.opened[control] = {};
                }

                $scope.opened[control][index] = true;
            }

        };

        $scope.canBeDeleted = function(){
            return !_.find($scope.certificaciondocumentalempresa.verificaciones, function(verificacion){
                return verificacion.cumplida;
            });            
        };

        $scope.$on('$destroy', function() {
            Menus.removeMenuItem('topbar', 'grupos/' + $scope.empresa.grupo._id);
            Menus.removeMenuItem('topbar', 'empresas/' + $scope.empresa._id);
        });

    }
]);
