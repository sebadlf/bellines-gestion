'use strict';

//Tipocertificaciontecnicabarcos service used to communicate Tipocertificaciontecnicabarcos REST endpoints
angular.module('tipocertificaciontecnicabarcos').factory('Tipocertificaciontecnicabarcos', ['$resource',
	function($resource) {
		return $resource('tipocertificaciontecnicabarcos/:tipocertificaciontecnicabarcoId', { tipocertificaciontecnicabarcoId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);