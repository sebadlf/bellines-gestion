'use strict';

// Tipocertificaciontecnicabarcos controller
angular.module('tipocertificaciontecnicabarcos').controller('TipocertificaciontecnicabarcosController', ['$scope', '$stateParams', '$location', 'Authentication', 'Tipocertificaciontecnicabarcos',
	function($scope, $stateParams, $location, Authentication, Tipocertificaciontecnicabarcos ) {
		$scope.authentication = Authentication;

		// Create new Tipocertificaciontecnicabarco
		$scope.create = function() {
			// Create new Tipocertificaciontecnicabarco object
			var tipocertificaciontecnicabarco = new Tipocertificaciontecnicabarcos ({
				nombre: this.nombre
			});

			// Redirect after save
			tipocertificaciontecnicabarco.$save(function(response) {
				$location.path('tipocertificaciontecnicabarcos/' + response._id);

				// Clear form fields
				$scope.nombre = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Tipocertificaciontecnicabarco
		$scope.remove = function( tipocertificaciontecnicabarco ) {
			if ( tipocertificaciontecnicabarco ) { tipocertificaciontecnicabarco.$remove();

				for (var i in $scope.tipocertificaciontecnicabarcos ) {
					if ($scope.tipocertificaciontecnicabarcos [i] === tipocertificaciontecnicabarco ) {
						$scope.tipocertificaciontecnicabarcos.splice(i, 1);
					}
				}
			} else {
				$scope.tipocertificaciontecnicabarco.$remove(function() {
					$location.path('tipocertificaciontecnicabarcos');
				});
			}
		};

		// Update existing Tipocertificaciontecnicabarco
		$scope.update = function() {
			var tipocertificaciontecnicabarco = $scope.tipocertificaciontecnicabarco ;

			tipocertificaciontecnicabarco.$update(function() {
				$location.path('tipocertificaciontecnicabarcos/' + tipocertificaciontecnicabarco._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Tipocertificaciontecnicabarcos
		$scope.find = function() {
			$scope.tipocertificaciontecnicabarcos = Tipocertificaciontecnicabarcos.query();
		};

		// Find existing Tipocertificaciontecnicabarco
		$scope.findOne = function() {
			$scope.tipocertificaciontecnicabarco = Tipocertificaciontecnicabarcos.get({ 
				tipocertificaciontecnicabarcoId: $stateParams.tipocertificaciontecnicabarcoId
			});
		};
	}
]);