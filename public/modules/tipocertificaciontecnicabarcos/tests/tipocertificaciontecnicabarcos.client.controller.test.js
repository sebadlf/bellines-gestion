'use strict';

(function() {
	// Tipocertificaciontecnicabarcos Controller Spec
	describe('Tipocertificaciontecnicabarcos Controller Tests', function() {
		// Initialize global variables
		var TipocertificaciontecnicabarcosController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Tipocertificaciontecnicabarcos controller.
			TipocertificaciontecnicabarcosController = $controller('TipocertificaciontecnicabarcosController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Tipocertificaciontecnicabarco object fetched from XHR', inject(function(Tipocertificaciontecnicabarcos) {
			// Create sample Tipocertificaciontecnicabarco using the Tipocertificaciontecnicabarcos service
			var sampleTipocertificaciontecnicabarco = new Tipocertificaciontecnicabarcos({
				name: 'New Tipocertificaciontecnicabarco'
			});

			// Create a sample Tipocertificaciontecnicabarcos array that includes the new Tipocertificaciontecnicabarco
			var sampleTipocertificaciontecnicabarcos = [sampleTipocertificaciontecnicabarco];

			// Set GET response
			$httpBackend.expectGET('tipocertificaciontecnicabarcos').respond(sampleTipocertificaciontecnicabarcos);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.tipocertificaciontecnicabarcos).toEqualData(sampleTipocertificaciontecnicabarcos);
		}));

		it('$scope.findOne() should create an array with one Tipocertificaciontecnicabarco object fetched from XHR using a tipocertificaciontecnicabarcoId URL parameter', inject(function(Tipocertificaciontecnicabarcos) {
			// Define a sample Tipocertificaciontecnicabarco object
			var sampleTipocertificaciontecnicabarco = new Tipocertificaciontecnicabarcos({
				name: 'New Tipocertificaciontecnicabarco'
			});

			// Set the URL parameter
			$stateParams.tipocertificaciontecnicabarcoId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/tipocertificaciontecnicabarcos\/([0-9a-fA-F]{24})$/).respond(sampleTipocertificaciontecnicabarco);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.tipocertificaciontecnicabarco).toEqualData(sampleTipocertificaciontecnicabarco);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Tipocertificaciontecnicabarcos) {
			// Create a sample Tipocertificaciontecnicabarco object
			var sampleTipocertificaciontecnicabarcoPostData = new Tipocertificaciontecnicabarcos({
				name: 'New Tipocertificaciontecnicabarco'
			});

			// Create a sample Tipocertificaciontecnicabarco response
			var sampleTipocertificaciontecnicabarcoResponse = new Tipocertificaciontecnicabarcos({
				_id: '525cf20451979dea2c000001',
				name: 'New Tipocertificaciontecnicabarco'
			});

			// Fixture mock form input values
			scope.name = 'New Tipocertificaciontecnicabarco';

			// Set POST response
			$httpBackend.expectPOST('tipocertificaciontecnicabarcos', sampleTipocertificaciontecnicabarcoPostData).respond(sampleTipocertificaciontecnicabarcoResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Tipocertificaciontecnicabarco was created
			expect($location.path()).toBe('/tipocertificaciontecnicabarcos/' + sampleTipocertificaciontecnicabarcoResponse._id);
		}));

		it('$scope.update() should update a valid Tipocertificaciontecnicabarco', inject(function(Tipocertificaciontecnicabarcos) {
			// Define a sample Tipocertificaciontecnicabarco put data
			var sampleTipocertificaciontecnicabarcoPutData = new Tipocertificaciontecnicabarcos({
				_id: '525cf20451979dea2c000001',
				name: 'New Tipocertificaciontecnicabarco'
			});

			// Mock Tipocertificaciontecnicabarco in scope
			scope.tipocertificaciontecnicabarco = sampleTipocertificaciontecnicabarcoPutData;

			// Set PUT response
			$httpBackend.expectPUT(/tipocertificaciontecnicabarcos\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/tipocertificaciontecnicabarcos/' + sampleTipocertificaciontecnicabarcoPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid tipocertificaciontecnicabarcoId and remove the Tipocertificaciontecnicabarco from the scope', inject(function(Tipocertificaciontecnicabarcos) {
			// Create new Tipocertificaciontecnicabarco object
			var sampleTipocertificaciontecnicabarco = new Tipocertificaciontecnicabarcos({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Tipocertificaciontecnicabarcos array and include the Tipocertificaciontecnicabarco
			scope.tipocertificaciontecnicabarcos = [sampleTipocertificaciontecnicabarco];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/tipocertificaciontecnicabarcos\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleTipocertificaciontecnicabarco);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.tipocertificaciontecnicabarcos.length).toBe(0);
		}));
	});
}());