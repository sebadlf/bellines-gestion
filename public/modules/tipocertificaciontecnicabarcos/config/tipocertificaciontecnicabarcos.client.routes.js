'use strict';

//Setting up route
angular.module('tipocertificaciontecnicabarcos').config(['$stateProvider',
	function($stateProvider) {
		// Tipocertificaciontecnicabarcos state routing
		$stateProvider.
		state('listTipocertificaciontecnicabarcos', {
			url: '/tipocertificaciontecnicabarcos',
			templateUrl: 'modules/tipocertificaciontecnicabarcos/views/list-tipocertificaciontecnicabarcos.client.view.html'
		}).
		state('createTipocertificaciontecnicabarco', {
			url: '/tipocertificaciontecnicabarcos/create',
			templateUrl: 'modules/tipocertificaciontecnicabarcos/views/create-tipocertificaciontecnicabarco.client.view.html'
		}).
		state('viewTipocertificaciontecnicabarco', {
			url: '/tipocertificaciontecnicabarcos/:tipocertificaciontecnicabarcoId',
			templateUrl: 'modules/tipocertificaciontecnicabarcos/views/view-tipocertificaciontecnicabarco.client.view.html'
		}).
		state('editTipocertificaciontecnicabarco', {
			url: '/tipocertificaciontecnicabarcos/:tipocertificaciontecnicabarcoId/edit',
			templateUrl: 'modules/tipocertificaciontecnicabarcos/views/edit-tipocertificaciontecnicabarco.client.view.html'
		});
	}
]);