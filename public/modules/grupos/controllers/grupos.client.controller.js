'use strict';

// Grupos controller
angular.module('grupos').controller('GruposController', ['$scope', '$stateParams', '$location', 'Authentication', 'Grupos', 'Menus', '$http', '$modal', '$log', 'Pagination', '$filter',
    function($scope, $stateParams, $location, Authentication, Grupos, Menus, $http, $modal, $log, Pagination, $filter) {
        $scope.authentication = Authentication;

        console.log(Authentication);

        $scope.pagination = Pagination.getNew();
        $scope.pagination = Pagination.getNew(5);

        // Create new Grupo
        $scope.create = function() {
            // Create new Grupo object
            var grupo = new Grupos({
                nombre: this.nombre,
                descripcion: this.descripcion
            });

            // Redirect after save
            grupo.$save(function(response) {
                $location.path('grupos/' + response._id);

                // Clear form fields
                $scope.nombre = '';
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Grupo
        $scope.remove = function(grupo) {

            var modalInstance = $modal.open({
                templateUrl: '/modules/grupos/views/delete-modal.html',
                size: 'sm',
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function() {

                if (grupo) {
                    grupo.$remove();

                    for (var i in $scope.grupos) {
                        if ($scope.grupos[i] === grupo) {
                            $scope.grupos.splice(i, 1);
                        }
                    }
                } else {
                    $scope.grupo.$remove(function() {
                        $location.path('grupos');
                    });
                }

            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });

        };

        // Update existing Grupo
        $scope.update = function() {
            var grupo = $scope.grupo;

            grupo.$update(function() {
                $location.path('grupos/' + grupo._id);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Grupos
        $scope.find = function() {
            $scope.grupos = Grupos.query();
        };

        // Find existing Grupo
        $scope.findOne = function() {
            $scope.grupo = Grupos.get({
                grupoId: $stateParams.grupoId
            });
        };

        $scope.getDashboardInfo = function() {
            var request = $http({
                    method: 'get',
                    url: '/dashboard'
                })
                .success(function(data) {
                    $scope.dashboardData = data;

                    $scope.pagination.numPages = Math.ceil(data.length/$scope.pagination.perPage);
                })
                .error(function(error) {
                    console.log(error);
                });
        };

        $scope.$watch('searchText', function(searchText){
            var data = $filter('multiWordFilter')($scope.dashboardData, searchText);
            if(data){
                $scope.pagination.numPages = Math.ceil(data.length/$scope.pagination.perPage);
            }
        });
    }
]);
