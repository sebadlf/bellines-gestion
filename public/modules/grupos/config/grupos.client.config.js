'use strict';

// Configuring the Articles module
angular.module('grupos').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Administración', 'grupos');
		Menus.addMenuItem('topbar', 'Tareas', 'dashboard');
	}
]);