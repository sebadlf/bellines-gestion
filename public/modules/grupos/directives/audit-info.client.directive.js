'use strict';

angular.module('grupos').directive('auditInfo', [
	function() {
		return {
			templateUrl: '/modules/grupos/views/audit-info.client.directive.html',
			restrict: 'E',
			scope: {
		        entity : '=entity'
		    },
			link: function postLink(scope, element, attrs) {
				// Audit info directive logic
				// ...

				
			}
		};
	}
]);