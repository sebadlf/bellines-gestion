'use strict';

(function() {
	// Certificaciondocumentalbarcos Controller Spec
	describe('Certificaciondocumentalbarcos Controller Tests', function() {
		// Initialize global variables
		var CertificaciondocumentalbarcosController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Certificaciondocumentalbarcos controller.
			CertificaciondocumentalbarcosController = $controller('CertificaciondocumentalbarcosController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Certificaciondocumentalbarco object fetched from XHR', inject(function(Certificaciondocumentalbarcos) {
			// Create sample Certificaciondocumentalbarco using the Certificaciondocumentalbarcos service
			var sampleCertificaciondocumentalbarco = new Certificaciondocumentalbarcos({
				name: 'New Certificaciondocumentalbarco'
			});

			// Create a sample Certificaciondocumentalbarcos array that includes the new Certificaciondocumentalbarco
			var sampleCertificaciondocumentalbarcos = [sampleCertificaciondocumentalbarco];

			// Set GET response
			$httpBackend.expectGET('certificaciondocumentalbarcos').respond(sampleCertificaciondocumentalbarcos);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.certificaciondocumentalbarcos).toEqualData(sampleCertificaciondocumentalbarcos);
		}));

		it('$scope.findOne() should create an array with one Certificaciondocumentalbarco object fetched from XHR using a certificaciondocumentalbarcoId URL parameter', inject(function(Certificaciondocumentalbarcos) {
			// Define a sample Certificaciondocumentalbarco object
			var sampleCertificaciondocumentalbarco = new Certificaciondocumentalbarcos({
				name: 'New Certificaciondocumentalbarco'
			});

			// Set the URL parameter
			$stateParams.certificaciondocumentalbarcoId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/certificaciondocumentalbarcos\/([0-9a-fA-F]{24})$/).respond(sampleCertificaciondocumentalbarco);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.certificaciondocumentalbarco).toEqualData(sampleCertificaciondocumentalbarco);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Certificaciondocumentalbarcos) {
			// Create a sample Certificaciondocumentalbarco object
			var sampleCertificaciondocumentalbarcoPostData = new Certificaciondocumentalbarcos({
				name: 'New Certificaciondocumentalbarco'
			});

			// Create a sample Certificaciondocumentalbarco response
			var sampleCertificaciondocumentalbarcoResponse = new Certificaciondocumentalbarcos({
				_id: '525cf20451979dea2c000001',
				name: 'New Certificaciondocumentalbarco'
			});

			// Fixture mock form input values
			scope.name = 'New Certificaciondocumentalbarco';

			// Set POST response
			$httpBackend.expectPOST('certificaciondocumentalbarcos', sampleCertificaciondocumentalbarcoPostData).respond(sampleCertificaciondocumentalbarcoResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Certificaciondocumentalbarco was created
			expect($location.path()).toBe('/certificaciondocumentalbarcos/' + sampleCertificaciondocumentalbarcoResponse._id);
		}));

		it('$scope.update() should update a valid Certificaciondocumentalbarco', inject(function(Certificaciondocumentalbarcos) {
			// Define a sample Certificaciondocumentalbarco put data
			var sampleCertificaciondocumentalbarcoPutData = new Certificaciondocumentalbarcos({
				_id: '525cf20451979dea2c000001',
				name: 'New Certificaciondocumentalbarco'
			});

			// Mock Certificaciondocumentalbarco in scope
			scope.certificaciondocumentalbarco = sampleCertificaciondocumentalbarcoPutData;

			// Set PUT response
			$httpBackend.expectPUT(/certificaciondocumentalbarcos\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/certificaciondocumentalbarcos/' + sampleCertificaciondocumentalbarcoPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid certificaciondocumentalbarcoId and remove the Certificaciondocumentalbarco from the scope', inject(function(Certificaciondocumentalbarcos) {
			// Create new Certificaciondocumentalbarco object
			var sampleCertificaciondocumentalbarco = new Certificaciondocumentalbarcos({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Certificaciondocumentalbarcos array and include the Certificaciondocumentalbarco
			scope.certificaciondocumentalbarcos = [sampleCertificaciondocumentalbarco];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/certificaciondocumentalbarcos\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleCertificaciondocumentalbarco);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.certificaciondocumentalbarcos.length).toBe(0);
		}));
	});
}());