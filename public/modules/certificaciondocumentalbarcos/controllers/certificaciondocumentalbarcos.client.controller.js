'use strict';

// CertificacionDocumentalBarcos controller
angular.module('certificaciondocumentalbarcos').controller('CertificacionDocumentalBarcosController', ['$scope', '$stateParams', '$location', 'Authentication', 'CertificacionDocumentalBarcos', 'Grupos', 'Barcos', '$filter', 'Menus', 'moment', 'lodash',
    function($scope, $stateParams, $location, Authentication, CertificacionDocumentalBarcos, Grupos, Barcos, $filter, Menus, moment, lodash) {
        $scope.authentication = Authentication;

        $scope.opened = {};

        $scope.barco = {};

        $scope.certificaciondocumentalbarco = {};

        $scope.isMenuGenerated = false;

        $scope.isNew = $stateParams.barcoId;

        var _ = lodash;


        var getBarcoInfo = function(barcoId) {
            $scope.barco = Barcos.get({
                    barcoId: barcoId
                })
                .$promise.then(function(barco) {
                    $scope.barco = barco;

                    if (!$scope.isMenuGenerated){

                        Menus.addMenuItem('topbar', 'Grupo', 'grupos/' + barco.grupo._id);
                        Menus.addMenuItem('topbar', 'Barco', 'barcos/' + $scope.barco._id);

                        $scope.isMenuGenerated = true;
                    }
                });
        };

        $scope.generarFechas = function() {

            var fechaDesde = $scope.certificaciondocumentalbarco.fechaDesde;

            console.log('generarFechas', fechaDesde);

            $scope.certificaciondocumentalbarco.fechaHasta = moment(fechaDesde).add(5, 'year').toDate();

            for (var i = 0; i <= $scope.certificaciondocumentalbarco.verificaciones.length - 1; i++) {
                var verificacion = $scope.certificaciondocumentalbarco.verificaciones[i];
                verificacion.fechaDesde = moment(fechaDesde).add(i + 2, 'year').toDate();
                verificacion.fechaHasta = moment(fechaDesde).add(i + 3, 'year').toDate();
            }
        };

        $scope.habilitarBotones = function(){

            function habilitarCumplidaFunction(verificacion) {

                return !_.find($scope.certificaciondocumentalbarco.verificaciones, function(otraVerificacion){
                    return otraVerificacion.anioVerificacion < verificacion.anioVerificacion && !otraVerificacion.cumplida;
                });

            }

            $scope.certificaciondocumentalbarco.habilitarCumplida = 
            !_.find($scope.certificaciondocumentalbarco.verificaciones, function(verificacion){
                return !verificacion.cumplida;
            });

            if (!$scope.certificaciondocumentalbarco.habilitarCumplida){
                $scope.certificaciondocumentalbarco.cumplida = false;
            }

            for (var i = 0; i < $scope.certificaciondocumentalbarco.verificaciones.length; i++) {
                var verificacion = $scope.certificaciondocumentalbarco.verificaciones[i];

                verificacion.habilitarCumplida = habilitarCumplidaFunction(verificacion);

                if (!verificacion.habilitarCumplida){
                    verificacion.cumplida = false;
                }
            }
        };

        if ($stateParams.barcoId) {
            getBarcoInfo($stateParams.barcoId);

            $scope.certificaciondocumentalbarco.verificaciones = [];

            for (var i = 1; i <= 1; i++) {
                $scope.certificaciondocumentalbarco.verificaciones.push({
                    nombre: 'Verificación de los Tres Años',
                    anioVerificacion: i
                });
            }

            $scope.certificaciondocumentalbarco.fechaDesde = new Date();

            $scope.generarFechas();

            $scope.habilitarBotones();
        }

        // Create new CertificacionDocumentalBarco
        $scope.create = function() {
            // Create new CertificacionDocumentalBarco object
            var certificaciondocumentalbarco = new CertificacionDocumentalBarcos({
                barco: $stateParams.barcoId,
                fechaDesde: $scope.certificaciondocumentalbarco.fechaDesde,
                fechaHasta: $scope.certificaciondocumentalbarco.fechaHasta,
                cumplida: $scope.certificaciondocumentalbarco.cumplida,
                comentarios: $scope.certificaciondocumentalbarco.comentarios,
                verificaciones: $scope.certificaciondocumentalbarco.verificaciones
            });

            // Redirect after save
            certificaciondocumentalbarco.$save(function(response) {
                $location.path('barcos/' + $scope.barco._id);

                // Clear form fields
                $scope.fechaDesde = null;
                $scope.fechaHasta = null;
                $scope.cumplida = false;
                $scope.comentarios = '';
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing CertificacionDocumentalBarco
        $scope.remove = function(certificaciondocumentalbarco) {
            if (certificaciondocumentalbarco) {
                certificaciondocumentalbarco.$remove();

                for (var i in $scope.certificaciondocumentalbarcos) {
                    if ($scope.certificaciondocumentalbarcos[i] === certificaciondocumentalbarco) {
                        $scope.certificaciondocumentalbarcos.splice(i, 1);
                    }
                }
            } else {
                $scope.certificaciondocumentalbarco.$remove(function() {
                    $location.path('barcos/' + $scope.barco._id);
                });
            }
        };

        // Update existing CertificacionDocumentalBarco
        $scope.update = function() {
            var certificaciondocumentalbarco = $scope.certificaciondocumentalbarco;

            certificaciondocumentalbarco.$update(function() {

                    $location.path('barcos/' + $scope.barco._id);
                
                    /*
                    $scope.certificaciondocumentalbarco.fechaDesde = new Date($scope.certificaciondocumentalbarco.fechaDesde);
                    $scope.certificaciondocumentalbarco.fechaHasta = new Date($scope.certificaciondocumentalbarco.fechaHasta);

                    for (var i = 0; i < certificaciondocumentalbarco.verificaciones.length; i++) {
                        var verificacion = certificaciondocumentalbarco.verificaciones[i];

                        verificacion.fechaDesde = new Date(verificacion.fechaDesde);
                        verificacion.fechaHasta = new Date(verificacion.fechaHasta);
                    }

                    $scope.habilitarBotones();

                    getBarcoInfo(certificaciondocumentalbarco.barco);                

                    */                  
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of CertificacionDocumentalBarcos
        $scope.find = function() {
            $scope.certificaciondocumentalbarcos = CertificacionDocumentalBarcos.query();
        };

        // Find existing CertificacionDocumentalBarco
        $scope.findOne = function() {
            CertificacionDocumentalBarcos.get({
                    certificaciondocumentalbarcoId: $stateParams.certificaciondocumentalbarcoId
                })
                .$promise.then(function(certificaciondocumentalbarco) {
                    $scope.certificaciondocumentalbarco = certificaciondocumentalbarco;

                    $scope.certificaciondocumentalbarco.fechaDesde = new Date($scope.certificaciondocumentalbarco.fechaDesde);
                    $scope.certificaciondocumentalbarco.fechaHasta = new Date($scope.certificaciondocumentalbarco.fechaHasta);

                    for (var i = 0; i < certificaciondocumentalbarco.verificaciones.length; i++) {
                        var verificacion = certificaciondocumentalbarco.verificaciones[i];

                        verificacion.fechaDesde = new Date(verificacion.fechaDesde);
                        verificacion.fechaHasta = new Date(verificacion.fechaHasta);
                    }

                    $scope.habilitarBotones();

                    getBarcoInfo(certificaciondocumentalbarco.barco);
                });
        };

        $scope.nuevaVerificacionComplementaria = function() {
            $scope.certificaciondocumentalbarco.verificaciones.push({
                nombre: 'Verificación Adicional',
                fechaDesde: new Date(),
                fechaHasta: moment(new Date()).add(3, 'month').toDate()
            });

            $scope.habilitarBotones();
        };

        $scope.eliminarVerificacionAnual = function(verificacionAnual) {
            verificacionAnual.deleted = true;

            console.log($scope.certificaciondocumentalbarco.verificaciones);

            $scope.certificaciondocumentalbarco.verificaciones =
            _.filter($scope.certificaciondocumentalbarco.verificaciones, function(verificacion){
                return !verificacion.deleted;
            });
        };

        $scope.open = function($event, control, index) {
            $event.preventDefault();
            $event.stopPropagation();

            console.log(control, index);

            if (index === undefined) {
                $scope.opened[control] = true;
            } else {
                if (!$scope.opened[control]) {
                    $scope.opened[control] = {};
                }

                $scope.opened[control][index] = true;
            }

        };

        $scope.canBeDeleted = function(){
            return !_.find($scope.certificaciondocumentalbarco.verificaciones, function(verificacion){
                return verificacion.cumplida;
            });            
        };        

        $scope.$on('$destroy', function() {
            Menus.removeMenuItem('topbar', 'grupos/' + $scope.barco.grupo._id);
            Menus.removeMenuItem('topbar', 'barcos/' + $scope.barco._id);
        });

    }
]);
