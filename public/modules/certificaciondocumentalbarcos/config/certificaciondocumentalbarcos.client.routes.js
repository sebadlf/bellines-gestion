'use strict';

//Setting up route
angular.module('certificaciondocumentalbarcos').config(['$stateProvider',
	function($stateProvider) {
		// CertificacionDocumentalBarcos state routing
		$stateProvider.
		state('listCertificacionDocumentalBarcos', {
			url: '/certificaciondocumentalbarcos',
			templateUrl: 'modules/certificaciondocumentalbarcos/views/list-certificaciondocumentalbarcos.client.view.html'
		}).
		state('createCertificacionDocumentalBarco', {
			url: '/certificaciondocumentalbarcos/create/:barcoId',
			templateUrl: 'modules/certificaciondocumentalbarcos/views/create-certificaciondocumentalbarco.client.view.html'
		}).
		state('viewCertificacionDocumentalBarco', {
			url: '/certificaciondocumentalbarcos/:certificaciondocumentalbarcoId',
			templateUrl: 'modules/certificaciondocumentalbarcos/views/view-certificaciondocumentalbarco.client.view.html'
		}).
		state('editCertificacionDocumentalBarco', {
			url: '/certificaciondocumentalbarcos/:certificaciondocumentalbarcoId/edit',
			templateUrl: 'modules/certificaciondocumentalbarcos/views/edit-certificaciondocumentalbarco.client.view.html'
		});
	}
]);