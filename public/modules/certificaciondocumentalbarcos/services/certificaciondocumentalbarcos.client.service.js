'use strict';

//CertificacionDocumentalBarcos service used to communicate CertificacionDocumentalBarcos REST endpoints
angular.module('certificaciondocumentalbarcos').factory('CertificacionDocumentalBarcos', ['$resource',
	function($resource) {
		return $resource('certificaciondocumentalbarcos/:certificaciondocumentalbarcoId', { certificaciondocumentalbarcoId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);