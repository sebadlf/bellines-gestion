'use strict';

//Setting up route
angular.module('barcos').config(['$stateProvider',
	function($stateProvider) {
		// Barcos state routing
		$stateProvider.
		state('listBarcos', {
			url: '/barcos',
			templateUrl: 'modules/barcos/views/list-barcos.client.view.html'
		}).
		state('createBarco', {
			url: '/barcos/create/:grupoId',
			templateUrl: 'modules/barcos/views/create-barco.client.view.html'
		}).
		state('viewBarco', {
			url: '/barcos/:barcoId',
			templateUrl: 'modules/barcos/views/view-barco.client.view.html'
		}).
		state('editBarco', {
			url: '/barcos/:barcoId/edit',
			templateUrl: 'modules/barcos/views/edit-barco.client.view.html'
		});
	}
]);