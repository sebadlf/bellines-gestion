'use strict';

(function() {
	// Barcos Controller Spec
	describe('Barcos Controller Tests', function() {
		// Initialize global variables
		var BarcosController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Barcos controller.
			BarcosController = $controller('BarcosController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Barco object fetched from XHR', inject(function(Barcos) {
			// Create sample Barco using the Barcos service
			var sampleBarco = new Barcos({
				name: 'New Barco'
			});

			// Create a sample Barcos array that includes the new Barco
			var sampleBarcos = [sampleBarco];

			// Set GET response
			$httpBackend.expectGET('barcos').respond(sampleBarcos);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.barcos).toEqualData(sampleBarcos);
		}));

		it('$scope.findOne() should create an array with one Barco object fetched from XHR using a barcoId URL parameter', inject(function(Barcos) {
			// Define a sample Barco object
			var sampleBarco = new Barcos({
				name: 'New Barco'
			});

			// Set the URL parameter
			$stateParams.barcoId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/barcos\/([0-9a-fA-F]{24})$/).respond(sampleBarco);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.barco).toEqualData(sampleBarco);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Barcos) {
			// Create a sample Barco object
			var sampleBarcoPostData = new Barcos({
				name: 'New Barco'
			});

			// Create a sample Barco response
			var sampleBarcoResponse = new Barcos({
				_id: '525cf20451979dea2c000001',
				name: 'New Barco'
			});

			// Fixture mock form input values
			scope.name = 'New Barco';

			// Set POST response
			$httpBackend.expectPOST('barcos', sampleBarcoPostData).respond(sampleBarcoResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Barco was created
			expect($location.path()).toBe('/barcos/' + sampleBarcoResponse._id);
		}));

		it('$scope.update() should update a valid Barco', inject(function(Barcos) {
			// Define a sample Barco put data
			var sampleBarcoPutData = new Barcos({
				_id: '525cf20451979dea2c000001',
				name: 'New Barco'
			});

			// Mock Barco in scope
			scope.barco = sampleBarcoPutData;

			// Set PUT response
			$httpBackend.expectPUT(/barcos\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/barcos/' + sampleBarcoPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid barcoId and remove the Barco from the scope', inject(function(Barcos) {
			// Create new Barco object
			var sampleBarco = new Barcos({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Barcos array and include the Barco
			scope.barcos = [sampleBarco];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/barcos\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleBarco);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.barcos.length).toBe(0);
		}));
	});
}());