'use strict';

//Barcos service used to communicate Barcos REST endpoints
angular.module('barcos').factory('Barcos', ['$resource',
	function($resource) {
		return $resource('barcos/:barcoId', { barcoId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);