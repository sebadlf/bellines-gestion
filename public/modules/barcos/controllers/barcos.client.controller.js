'use strict';

// Barcos controller
angular.module('barcos').controller('BarcosController', ['$scope', '$stateParams', '$location', 'Authentication', 'Barcos', 'Grupos', 'Menus', '$modal', '$log', 'lodash',
    function($scope, $stateParams, $location, Authentication, Barcos, Grupos, Menus, $modal, $log, lodash) {
        $scope.authentication = Authentication;

        var _ = lodash;

        var grupoId = $stateParams.grupoId;

        if (grupoId) {
            Menus.addMenuItem('topbar', 'Grupo', 'grupos/' + grupoId);
        }

        // Create new Barco
        $scope.create = function() {

            // Create new Barco object
            var barco = new Barcos({
                grupo: $stateParams.grupoId,
                nombre: this.nombre
            });

            // Redirect after save
            barco.$save(function(response) {
                $location.path('barcos/' + response._id);

                // Clear form fields
                $scope.nombre = '';
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Barco
        $scope.remove = function(barco) {

            var modalInstance = $modal.open({
                templateUrl: '/modules/grupos/views/delete-modal.html',
                size: 'sm',
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function() {
                if (barco) {
                    barco.$remove();

                    for (var i in $scope.barcos) {
                        if ($scope.barcos[i] === barco) {
                            $scope.barcos.splice(i, 1);
                        }
                    }
                } else {
                    $scope.barco.$remove(function() {
                        $location.path('barcos');
                    });
                }

            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        // Update existing Barco
        $scope.update = function() {
            var barco = $scope.barco;

            barco.$update(function() {
                $location.path('barcos/' + barco._id);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Barcos
        $scope.find = function() {
            $scope.barcos = Barcos.query();
        };

        // Find existing Barco
        $scope.findOne = function() {
            Barcos.get({
                    barcoId: $stateParams.barcoId
                })
                .$promise.then(function(result) {
                    $scope.barco = result;
                    grupoId = result.grupo._id;
                    Menus.addMenuItem('topbar', 'Grupo', 'grupos/' + grupoId);
                });
        };

        $scope.hasCertificacionDocumentalPorCumplir = function(){
            return $scope.barco && _.findWhere($scope.barco.certificacionesDocumentales, { cumplida: false });
        };

        $scope.hasCertificacionTecnicaPorCumplir = function(){
            return $scope.barco && _.findWhere($scope.barco.certificacionesTecnicas, { cumplida: false });
        };            

        $scope.$on('$destroy', function() {
            Menus.removeMenuItem('topbar', 'grupos/' + grupoId);
        });
    }
]);
