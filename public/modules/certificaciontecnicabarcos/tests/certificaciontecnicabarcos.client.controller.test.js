'use strict';

(function() {
	// Certificaciontecnicabarcos Controller Spec
	describe('Certificaciontecnicabarcos Controller Tests', function() {
		// Initialize global variables
		var CertificaciontecnicabarcosController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Certificaciontecnicabarcos controller.
			CertificaciontecnicabarcosController = $controller('CertificaciontecnicabarcosController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Certificaciontecnicabarco object fetched from XHR', inject(function(Certificaciontecnicabarcos) {
			// Create sample Certificaciontecnicabarco using the Certificaciontecnicabarcos service
			var sampleCertificaciontecnicabarco = new Certificaciontecnicabarcos({
				name: 'New Certificaciontecnicabarco'
			});

			// Create a sample Certificaciontecnicabarcos array that includes the new Certificaciontecnicabarco
			var sampleCertificaciontecnicabarcos = [sampleCertificaciontecnicabarco];

			// Set GET response
			$httpBackend.expectGET('certificaciontecnicabarcos').respond(sampleCertificaciontecnicabarcos);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.certificaciontecnicabarcos).toEqualData(sampleCertificaciontecnicabarcos);
		}));

		it('$scope.findOne() should create an array with one Certificaciontecnicabarco object fetched from XHR using a certificaciontecnicabarcoId URL parameter', inject(function(Certificaciontecnicabarcos) {
			// Define a sample Certificaciontecnicabarco object
			var sampleCertificaciontecnicabarco = new Certificaciontecnicabarcos({
				name: 'New Certificaciontecnicabarco'
			});

			// Set the URL parameter
			$stateParams.certificaciontecnicabarcoId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/certificaciontecnicabarcos\/([0-9a-fA-F]{24})$/).respond(sampleCertificaciontecnicabarco);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.certificaciontecnicabarco).toEqualData(sampleCertificaciontecnicabarco);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Certificaciontecnicabarcos) {
			// Create a sample Certificaciontecnicabarco object
			var sampleCertificaciontecnicabarcoPostData = new Certificaciontecnicabarcos({
				name: 'New Certificaciontecnicabarco'
			});

			// Create a sample Certificaciontecnicabarco response
			var sampleCertificaciontecnicabarcoResponse = new Certificaciontecnicabarcos({
				_id: '525cf20451979dea2c000001',
				name: 'New Certificaciontecnicabarco'
			});

			// Fixture mock form input values
			scope.name = 'New Certificaciontecnicabarco';

			// Set POST response
			$httpBackend.expectPOST('certificaciontecnicabarcos', sampleCertificaciontecnicabarcoPostData).respond(sampleCertificaciontecnicabarcoResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Certificaciontecnicabarco was created
			expect($location.path()).toBe('/certificaciontecnicabarcos/' + sampleCertificaciontecnicabarcoResponse._id);
		}));

		it('$scope.update() should update a valid Certificaciontecnicabarco', inject(function(Certificaciontecnicabarcos) {
			// Define a sample Certificaciontecnicabarco put data
			var sampleCertificaciontecnicabarcoPutData = new Certificaciontecnicabarcos({
				_id: '525cf20451979dea2c000001',
				name: 'New Certificaciontecnicabarco'
			});

			// Mock Certificaciontecnicabarco in scope
			scope.certificaciontecnicabarco = sampleCertificaciontecnicabarcoPutData;

			// Set PUT response
			$httpBackend.expectPUT(/certificaciontecnicabarcos\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/certificaciontecnicabarcos/' + sampleCertificaciontecnicabarcoPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid certificaciontecnicabarcoId and remove the Certificaciontecnicabarco from the scope', inject(function(Certificaciontecnicabarcos) {
			// Create new Certificaciontecnicabarco object
			var sampleCertificaciontecnicabarco = new Certificaciontecnicabarcos({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Certificaciontecnicabarcos array and include the Certificaciontecnicabarco
			scope.certificaciontecnicabarcos = [sampleCertificaciontecnicabarco];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/certificaciontecnicabarcos\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleCertificaciontecnicabarco);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.certificaciontecnicabarcos.length).toBe(0);
		}));
	});
}());