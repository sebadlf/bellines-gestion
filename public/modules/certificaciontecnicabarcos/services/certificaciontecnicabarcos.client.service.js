'use strict';

//CertificacionTecnicaBarcos service used to communicate CertificacionTecnicaBarcos REST endpoints
angular.module('certificaciontecnicabarcos').factory('CertificacionTecnicaBarcos', ['$resource',
	function($resource) {
		return $resource('certificaciontecnicabarcos/:certificaciontecnicabarcoId', { certificaciontecnicabarcoId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);