'use strict';

//Setting up route
angular.module('certificaciontecnicabarcos').config(['$stateProvider',
	function($stateProvider) {
		// CertificacionTecnicaBarcos state routing
		$stateProvider.
		state('listCertificacionTecnicaBarcos', {
			url: '/certificaciontecnicabarcos',
			templateUrl: 'modules/certificaciontecnicabarcos/views/list-certificaciontecnicabarcos.client.view.html'
		}).
		state('createCertificacionTecnicaBarco', {
			url: '/certificaciontecnicabarcos/create/:barcoId',
			templateUrl: 'modules/certificaciontecnicabarcos/views/create-certificaciontecnicabarco.client.view.html'
		}).
		state('viewCertificacionTecnicaBarco', {
			url: '/certificaciontecnicabarcos/:certificaciontecnicabarcoId',
			templateUrl: 'modules/certificaciontecnicabarcos/views/view-certificaciontecnicabarco.client.view.html'
		}).
		state('editCertificacionTecnicaBarco', {
			url: '/certificaciontecnicabarcos/:certificaciontecnicabarcoId/edit',
			templateUrl: 'modules/certificaciontecnicabarcos/views/edit-certificaciontecnicabarco.client.view.html'
		});
	}
]);