'use strict';

// CertificacionTecnicaBarcos controller
angular.module('certificaciontecnicabarcos').controller('CertificacionTecnicaBarcosController', ['$scope', '$stateParams', '$location', 'Authentication', 'CertificacionTecnicaBarcos', 'Tipocertificaciontecnicabarcos', 'Grupos', 'Barcos', '$filter', 'Menus', 'moment', 'lodash',
    function($scope, $stateParams, $location, Authentication, CertificacionTecnicaBarcos, Tipocertificaciontecnicabarcos, Grupos, Barcos, $filter, Menus, moment, lodash) {
        $scope.authentication = Authentication;

        $scope.opened = {};

        $scope.barco = {};

        $scope.certificaciontecnicabarco = {};

        $scope.isMenuGenerated = false;

        $scope.isNew = $stateParams.barcoId;

        var verificacionesConfig = [{
            nombre: 'Verificación Intermedia Casco en Seco',
            sumarMeses: 36,
            dispersionMeses: 6,
            tipoVerifiaccion: 1,
            anioVerificacion: 1
        }, {
            nombre: 'Verificación Intermedia de Máquinas',
            sumarMeses: 36,
            dispersionMeses: 6,
            tipoVerifiaccion: 2,
            anioVerificacion: 1
        }, {
            nombre: 'Verificación Intermedia de Electricidad',
            sumarMeses: 36,
            dispersionMeses: 6,
            tipoVerifiaccion: 3,
            anioVerificacion: 1
        }, {
            nombre: '1° Verificación Complementaria de Armamento',
            sumarMeses: 18,
            dispersionMeses: 3,
            tipoVerifiaccion: 4,
            anioVerificacion: 1
        }, {
            nombre: '1° Verificación Complementaria de Radio',
            sumarMeses: 18,
            dispersionMeses: 3,
            tipoVerifiaccion: 4,
            anioVerificacion: 2
        }, {
            nombre: '2° Verificación Complementaria de Armamento',
            sumarMeses: 54,
            dispersionMeses: 3,
            tipoVerifiaccion: 5,
            anioVerificacion: 1
        }, {
            nombre: '2° Verificación Complementaria de Radio',
            sumarMeses: 54,
            dispersionMeses: 3,
            tipoVerifiaccion: 5,
            anioVerificacion: 2
        }];

        var _ = lodash;

        var getBarcoInfo = function(barcoId) {
            $scope.barco = Barcos.get({
                    barcoId: barcoId
                })
                .$promise.then(function(barco) {
                    $scope.barco = barco;

                    if (!$scope.isMenuGenerated){

                        Menus.addMenuItem('topbar', 'Grupo', 'grupos/' + barco.grupo._id);
                        Menus.addMenuItem('topbar', 'Barco', 'barcos/' + $scope.barco._id);

                        $scope.isMenuGenerated = true;
                    }
                });

            $scope.tipoCertificacionTecnicaBarcosList = Tipocertificaciontecnicabarcos.query();
        };

        $scope.generarFechas = function() {

            var fechaDesde = $scope.certificaciontecnicabarco.fechaDesde;

            console.log('generarFechas', fechaDesde);

            $scope.certificaciontecnicabarco.fechaHasta = moment(fechaDesde).add(6, 'year').toDate();

            for (var i = 0; i <= verificacionesConfig.length - 1; i++) {
                var verificacionConfig = verificacionesConfig[i];
                var verificacion = $scope.certificaciontecnicabarco.verificaciones[i];

                verificacion.fechaDesde = moment(fechaDesde).add(verificacionConfig.sumarMeses - verificacionConfig.dispersionMeses, 'month').toDate();
                verificacion.fechaHasta = moment(fechaDesde).add(verificacionConfig.sumarMeses + verificacionConfig.dispersionMeses, 'month').toDate();
            }
        };

        $scope.habilitarBotones = function(){

            function habilitarCumplidaFunction(verificacion) {

                return !_.find($scope.certificaciontecnicabarco.verificaciones, function(otraVerificacion){
                    return otraVerificacion.tipoVerifiaccion === verificacion.tipoVerifiaccion &&
                        otraVerificacion.anioVerificacion < verificacion.anioVerificacion &&
                        !otraVerificacion.cumplida;
                });

            }

            $scope.certificaciontecnicabarco.habilitarCumplida =
            !_.find($scope.certificaciontecnicabarco.verificaciones, function(verificacion){
                return !verificacion.cumplida;
            });

            if (!$scope.certificaciontecnicabarco.habilitarCumplida){
                $scope.certificaciontecnicabarco.cumplida = false;
            }

            for (var i = 0; i < $scope.certificaciontecnicabarco.verificaciones.length; i++) {
                var verificacion = $scope.certificaciontecnicabarco.verificaciones[i];

                verificacion.habilitarCumplida = habilitarCumplidaFunction(verificacion);

                if (!verificacion.habilitarCumplida){
                    verificacion.cumplida = false;
                }
            }
        };

        if ($stateParams.barcoId) {
            getBarcoInfo($stateParams.barcoId);

            $scope.certificaciontecnicabarco.verificaciones = [];

            $scope.certificaciontecnicabarco.fechaDesde = new Date();

            for (var i = 0; i < verificacionesConfig.length; i++) {
                var verificacionConfig = verificacionesConfig[i];

                $scope.certificaciontecnicabarco.verificaciones.push({
                    nombre: verificacionConfig.nombre,
                    tipoVerifiaccion: verificacionConfig.tipoVerifiaccion,
                    anioVerificacion: verificacionConfig.anioVerificacion
                });
            }

            $scope.generarFechas();

            $scope.habilitarBotones();
        }

        // Create new CertificacionTecnicaBarco
        $scope.create = function() {
            // Create new CertificacionTecnicaBarco object
            var certificaciontecnicabarco = new CertificacionTecnicaBarcos({
                barco: $stateParams.barcoId,
                fechaDesde: $scope.certificaciontecnicabarco.fechaDesde,
                fechaHasta: $scope.certificaciontecnicabarco.fechaHasta,
                cumplida: $scope.certificaciontecnicabarco.cumplida,
                comentarios: $scope.certificaciontecnicabarco.comentarios,
                verificaciones: $scope.certificaciontecnicabarco.verificaciones
            });

            // Redirect after save
            certificaciontecnicabarco.$save(function(response) {
                $location.path('barcos/' + $scope.barco._id);

                // Clear form fields
                $scope.fechaDesde = null;
                $scope.fechaHasta = null;
                $scope.cumplida = false;
                $scope.comentarios = '';
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing CertificacionTecnicaBarco
        $scope.remove = function(certificaciontecnicabarco) {
            if (certificaciontecnicabarco) {
                certificaciontecnicabarco.$remove();

                for (var i in $scope.certificaciontecnicabarcos) {
                    if ($scope.certificaciontecnicabarcos[i] === certificaciontecnicabarco) {
                        $scope.certificaciontecnicabarcos.splice(i, 1);
                    }
                }
            } else {
                $scope.certificaciontecnicabarco.$remove(function() {
                    $location.path('barcos/' + $scope.barco._id);
                });
            }
        };

        // Update existing CertificacionTecnicaBarco
        $scope.update = function() {
            var certificaciontecnicabarco = $scope.certificaciontecnicabarco;

            certificaciontecnicabarco.$update(function() {

                    $location.path('barcos/' + $scope.barco._id);

            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of CertificacionTecnicaBarcos
        $scope.find = function() {
            $scope.certificaciontecnicabarcos = CertificacionTecnicaBarcos.query();
        };

        // Find existing CertificacionTecnicaBarco
        $scope.findOne = function() {
            CertificacionTecnicaBarcos.get({
                    certificaciontecnicabarcoId: $stateParams.certificaciontecnicabarcoId
                })
                .$promise.then(function(certificaciontecnicabarco) {
                    $scope.certificaciontecnicabarco = certificaciontecnicabarco;

                    $scope.certificaciontecnicabarco.fechaDesde = new Date($scope.certificaciontecnicabarco.fechaDesde);
                    $scope.certificaciontecnicabarco.fechaHasta = new Date($scope.certificaciontecnicabarco.fechaHasta);

                    for (var i = 0; i < certificaciontecnicabarco.verificaciones.length; i++) {
                        var verificacion = certificaciontecnicabarco.verificaciones[i];

                        verificacion.fechaDesde = new Date(verificacion.fechaDesde);
                        verificacion.fechaHasta = new Date(verificacion.fechaHasta);
                    }

                    $scope.habilitarBotones();

                    getBarcoInfo(certificaciontecnicabarco.barco);
                });
        };

        $scope.nuevaVerificacionComplementaria = function() {
            $scope.certificaciontecnicabarco.verificaciones.push({
                nombre: 'Verificación Adicional',
                fechaDesde: new Date(),
                fechaHasta: moment(new Date()).add(3, 'month').toDate()
            });

            $scope.habilitarBotones();
        };

        $scope.eliminarVerificacionAnual = function(verificacionAnual) {
            verificacionAnual.deleted = true;

            console.log($scope.certificaciontecnicabarco.verificaciones);

            $scope.certificaciontecnicabarco.verificaciones =
            _.filter($scope.certificaciontecnicabarco.verificaciones, function(verificacion){
                return !verificacion.deleted;
            });
        };

        $scope.open = function($event, control, index) {
            $event.preventDefault();
            $event.stopPropagation();

            console.log(control, index);

            if (index === undefined) {
                $scope.opened[control] = true;
            } else {
                if (!$scope.opened[control]) {
                    $scope.opened[control] = {};
                }

                $scope.opened[control][index] = true;
            }

        };

        $scope.canBeDeleted = function(){
            return !_.find($scope.certificaciontecnicabarco.verificaciones, function(verificacion){
                return verificacion.cumplida;
            });
        };

        $scope.$on('$destroy', function() {
            Menus.removeMenuItem('topbar', 'grupos/' + $scope.barco.grupo._id);
            Menus.removeMenuItem('topbar', 'barcos/' + $scope.barco._id);
        });

    }
]);
