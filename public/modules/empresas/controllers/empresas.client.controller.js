'use strict';

// Empresas controller
angular.module('empresas').controller('EmpresasController', ['$scope', '$stateParams', '$location', 'Authentication', 'Empresas', 'Grupos', 'Menus', '$modal', '$log', 'lodash',
    function($scope, $stateParams, $location, Authentication, Empresas, Grupos, Menus, $modal, $log, lodash) {
        $scope.authentication = Authentication;

        var _ = lodash;

        var grupoId = $stateParams.grupoId;

        if (grupoId) {
            Menus.addMenuItem('topbar', 'Grupo', 'grupos/' + grupoId);
        }

        // Create new Empresa
        $scope.create = function() {

            // Create new Empresa object
            var empresa = new Empresas({
                grupo: $stateParams.grupoId,
                nombre: this.nombre
            });

            // Redirect after save
            empresa.$save(function(response) {
                $location.path('empresas/' + response._id);

                // Clear form fields
                $scope.nombre = '';
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Empresa
        $scope.remove = function(empresa) {

            var modalInstance = $modal.open({
                templateUrl: '/modules/grupos/views/delete-modal.html',
                size: 'sm',
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function() {
                if (empresa) {
                    empresa.$remove();

                    for (var i in $scope.empresas) {
                        if ($scope.empresas[i] === empresa) {
                            $scope.empresas.splice(i, 1);
                        }
                    }
                } else {
                    $scope.empresa.$remove(function() {
                        $location.path('empresas');
                    });
                }

            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        // Update existing Empresa
        $scope.update = function() {
            var empresa = $scope.empresa;

            empresa.$update(function() {
                $location.path('empresas/' + empresa._id);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Empresas
        $scope.find = function() {
            $scope.empresas = Empresas.query();
        };

        // Find existing Empresa
        $scope.findOne = function() {
            Empresas.get({
                    empresaId: $stateParams.empresaId
                })
                .$promise.then(function(result) {
                    $scope.empresa = result;
                    grupoId = result.grupo._id;
                    Menus.addMenuItem('topbar', 'Grupo', 'grupos/' + grupoId);
                });
        };

        $scope.hasCertificacionPorCumplir = function(){
            return $scope.empresa && _.findWhere($scope.empresa.certificacionesDocumentales, { cumplida: false });
        };        

        $scope.$on('$destroy', function() {
            Menus.removeMenuItem('topbar', 'grupos/' + grupoId);
        });
    }
]);
