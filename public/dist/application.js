'use strict';
// Init the application configuration module for AngularJS application
var ApplicationConfiguration = function () {
    // Init module configuration options
    var applicationModuleName = 'bellines-gestion';
    var applicationModuleVendorDependencies = [
        'ngResource',
        'ngCookies',
        'ngAnimate',
        'ngTouch',
        'ngSanitize',
        'ui.router',
        'ui.bootstrap',
        'ui.utils',
        'angularMoment',
        'ngLodash',
        'simplePagination'
      ];
    // Add a new vertical module
    var registerModule = function (moduleName, dependencies) {
      // Create angular module
      angular.module(moduleName, dependencies || []);
      // Add the module to the AngularJS configuration file
      angular.module(applicationModuleName).requires.push(moduleName);
    };
    return {
      applicationModuleName: applicationModuleName,
      applicationModuleVendorDependencies: applicationModuleVendorDependencies,
      registerModule: registerModule
    };
  }();'use strict';
//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);
// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config([
  '$locationProvider',
  function ($locationProvider) {
    $locationProvider.hashPrefix('!');
  }
]);
//Then define the init function for starting up the application
angular.element(document).ready(function () {
  //Fixing facebook bug with redirect
  if (window.location.hash === '#_=_')
    window.location.hash = '#!';
  //Then init the app
  angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('barcos');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('certificaciondocumentalbarcos');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('certificaciondocumentalempresas');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('certificaciontecnicabarcos');'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('empresas');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('grupos');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('tipocertificaciontecnicabarcos');'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users');'use strict';
//Setting up route
angular.module('barcos').config([
  '$stateProvider',
  function ($stateProvider) {
    // Barcos state routing
    $stateProvider.state('listBarcos', {
      url: '/barcos',
      templateUrl: 'modules/barcos/views/list-barcos.client.view.html'
    }).state('createBarco', {
      url: '/barcos/create/:grupoId',
      templateUrl: 'modules/barcos/views/create-barco.client.view.html'
    }).state('viewBarco', {
      url: '/barcos/:barcoId',
      templateUrl: 'modules/barcos/views/view-barco.client.view.html'
    }).state('editBarco', {
      url: '/barcos/:barcoId/edit',
      templateUrl: 'modules/barcos/views/edit-barco.client.view.html'
    });
  }
]);'use strict';
// Barcos controller
angular.module('barcos').controller('BarcosController', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'Barcos',
  'Grupos',
  'Menus',
  '$modal',
  '$log',
  'lodash',
  function ($scope, $stateParams, $location, Authentication, Barcos, Grupos, Menus, $modal, $log, lodash) {
    $scope.authentication = Authentication;
    var _ = lodash;
    var grupoId = $stateParams.grupoId;
    if (grupoId) {
      Menus.addMenuItem('topbar', 'Grupo', 'grupos/' + grupoId);
    }
    // Create new Barco
    $scope.create = function () {
      // Create new Barco object
      var barco = new Barcos({
          grupo: $stateParams.grupoId,
          nombre: this.nombre
        });
      // Redirect after save
      barco.$save(function (response) {
        $location.path('barcos/' + response._id);
        // Clear form fields
        $scope.nombre = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Remove existing Barco
    $scope.remove = function (barco) {
      var modalInstance = $modal.open({
          templateUrl: '/modules/grupos/views/delete-modal.html',
          size: 'sm',
          resolve: {
            items: function () {
              return $scope.items;
            }
          }
        });
      modalInstance.result.then(function () {
        if (barco) {
          barco.$remove();
          for (var i in $scope.barcos) {
            if ($scope.barcos[i] === barco) {
              $scope.barcos.splice(i, 1);
            }
          }
        } else {
          $scope.barco.$remove(function () {
            $location.path('barcos');
          });
        }
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };
    // Update existing Barco
    $scope.update = function () {
      var barco = $scope.barco;
      barco.$update(function () {
        $location.path('barcos/' + barco._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Find a list of Barcos
    $scope.find = function () {
      $scope.barcos = Barcos.query();
    };
    // Find existing Barco
    $scope.findOne = function () {
      Barcos.get({ barcoId: $stateParams.barcoId }).$promise.then(function (result) {
        $scope.barco = result;
        grupoId = result.grupo._id;
        Menus.addMenuItem('topbar', 'Grupo', 'grupos/' + grupoId);
      });
    };
    $scope.hasCertificacionDocumentalPorCumplir = function () {
      return $scope.barco && _.findWhere($scope.barco.certificacionesDocumentales, { cumplida: false });
    };
    $scope.hasCertificacionTecnicaPorCumplir = function () {
      return $scope.barco && _.findWhere($scope.barco.certificacionesTecnicas, { cumplida: false });
    };
    $scope.$on('$destroy', function () {
      Menus.removeMenuItem('topbar', 'grupos/' + grupoId);
    });
  }
]);'use strict';
//Barcos service used to communicate Barcos REST endpoints
angular.module('barcos').factory('Barcos', [
  '$resource',
  function ($resource) {
    return $resource('barcos/:barcoId', { barcoId: '@_id' }, { update: { method: 'PUT' } });
  }
]);'use strict';
//Setting up route
angular.module('certificaciondocumentalbarcos').config([
  '$stateProvider',
  function ($stateProvider) {
    // CertificacionDocumentalBarcos state routing
    $stateProvider.state('listCertificacionDocumentalBarcos', {
      url: '/certificaciondocumentalbarcos',
      templateUrl: 'modules/certificaciondocumentalbarcos/views/list-certificaciondocumentalbarcos.client.view.html'
    }).state('createCertificacionDocumentalBarco', {
      url: '/certificaciondocumentalbarcos/create/:barcoId',
      templateUrl: 'modules/certificaciondocumentalbarcos/views/create-certificaciondocumentalbarco.client.view.html'
    }).state('viewCertificacionDocumentalBarco', {
      url: '/certificaciondocumentalbarcos/:certificaciondocumentalbarcoId',
      templateUrl: 'modules/certificaciondocumentalbarcos/views/view-certificaciondocumentalbarco.client.view.html'
    }).state('editCertificacionDocumentalBarco', {
      url: '/certificaciondocumentalbarcos/:certificaciondocumentalbarcoId/edit',
      templateUrl: 'modules/certificaciondocumentalbarcos/views/edit-certificaciondocumentalbarco.client.view.html'
    });
  }
]);'use strict';
// CertificacionDocumentalBarcos controller
angular.module('certificaciondocumentalbarcos').controller('CertificacionDocumentalBarcosController', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'CertificacionDocumentalBarcos',
  'Grupos',
  'Barcos',
  '$filter',
  'Menus',
  'moment',
  'lodash',
  function ($scope, $stateParams, $location, Authentication, CertificacionDocumentalBarcos, Grupos, Barcos, $filter, Menus, moment, lodash) {
    $scope.authentication = Authentication;
    $scope.opened = {};
    $scope.barco = {};
    $scope.certificaciondocumentalbarco = {};
    $scope.isMenuGenerated = false;
    $scope.isNew = $stateParams.barcoId;
    var _ = lodash;
    var getBarcoInfo = function (barcoId) {
      $scope.barco = Barcos.get({ barcoId: barcoId }).$promise.then(function (barco) {
        $scope.barco = barco;
        if (!$scope.isMenuGenerated) {
          Menus.addMenuItem('topbar', 'Grupo', 'grupos/' + barco.grupo._id);
          Menus.addMenuItem('topbar', 'Barco', 'barcos/' + $scope.barco._id);
          $scope.isMenuGenerated = true;
        }
      });
    };
    $scope.generarFechas = function () {
      var fechaDesde = $scope.certificaciondocumentalbarco.fechaDesde;
      console.log('generarFechas', fechaDesde);
      $scope.certificaciondocumentalbarco.fechaHasta = moment(fechaDesde).add(5, 'year').toDate();
      for (var i = 0; i <= $scope.certificaciondocumentalbarco.verificaciones.length - 1; i++) {
        var verificacion = $scope.certificaciondocumentalbarco.verificaciones[i];
        verificacion.fechaDesde = moment(fechaDesde).add(i + 2, 'year').toDate();
        verificacion.fechaHasta = moment(fechaDesde).add(i + 3, 'year').toDate();
      }
    };
    $scope.habilitarBotones = function () {
      function habilitarCumplidaFunction(verificacion) {
        return !_.find($scope.certificaciondocumentalbarco.verificaciones, function (otraVerificacion) {
          return otraVerificacion.anioVerificacion < verificacion.anioVerificacion && !otraVerificacion.cumplida;
        });
      }
      $scope.certificaciondocumentalbarco.habilitarCumplida = !_.find($scope.certificaciondocumentalbarco.verificaciones, function (verificacion) {
        return !verificacion.cumplida;
      });
      if (!$scope.certificaciondocumentalbarco.habilitarCumplida) {
        $scope.certificaciondocumentalbarco.cumplida = false;
      }
      for (var i = 0; i < $scope.certificaciondocumentalbarco.verificaciones.length; i++) {
        var verificacion = $scope.certificaciondocumentalbarco.verificaciones[i];
        verificacion.habilitarCumplida = habilitarCumplidaFunction(verificacion);
        if (!verificacion.habilitarCumplida) {
          verificacion.cumplida = false;
        }
      }
    };
    if ($stateParams.barcoId) {
      getBarcoInfo($stateParams.barcoId);
      $scope.certificaciondocumentalbarco.verificaciones = [];
      for (var i = 1; i <= 1; i++) {
        $scope.certificaciondocumentalbarco.verificaciones.push({
          nombre: 'Verificaci\xf3n de los Tres A\xf1os',
          anioVerificacion: i
        });
      }
      $scope.certificaciondocumentalbarco.fechaDesde = new Date();
      $scope.generarFechas();
      $scope.habilitarBotones();
    }
    // Create new CertificacionDocumentalBarco
    $scope.create = function () {
      // Create new CertificacionDocumentalBarco object
      var certificaciondocumentalbarco = new CertificacionDocumentalBarcos({
          barco: $stateParams.barcoId,
          fechaDesde: $scope.certificaciondocumentalbarco.fechaDesde,
          fechaHasta: $scope.certificaciondocumentalbarco.fechaHasta,
          cumplida: $scope.certificaciondocumentalbarco.cumplida,
          comentarios: $scope.certificaciondocumentalbarco.comentarios,
          verificaciones: $scope.certificaciondocumentalbarco.verificaciones
        });
      // Redirect after save
      certificaciondocumentalbarco.$save(function (response) {
        $location.path('barcos/' + $scope.barco._id);
        // Clear form fields
        $scope.fechaDesde = null;
        $scope.fechaHasta = null;
        $scope.cumplida = false;
        $scope.comentarios = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Remove existing CertificacionDocumentalBarco
    $scope.remove = function (certificaciondocumentalbarco) {
      if (certificaciondocumentalbarco) {
        certificaciondocumentalbarco.$remove();
        for (var i in $scope.certificaciondocumentalbarcos) {
          if ($scope.certificaciondocumentalbarcos[i] === certificaciondocumentalbarco) {
            $scope.certificaciondocumentalbarcos.splice(i, 1);
          }
        }
      } else {
        $scope.certificaciondocumentalbarco.$remove(function () {
          $location.path('barcos/' + $scope.barco._id);
        });
      }
    };
    // Update existing CertificacionDocumentalBarco
    $scope.update = function () {
      var certificaciondocumentalbarco = $scope.certificaciondocumentalbarco;
      certificaciondocumentalbarco.$update(function () {
        $location.path('barcos/' + $scope.barco._id);  /*
                    $scope.certificaciondocumentalbarco.fechaDesde = new Date($scope.certificaciondocumentalbarco.fechaDesde);
                    $scope.certificaciondocumentalbarco.fechaHasta = new Date($scope.certificaciondocumentalbarco.fechaHasta);

                    for (var i = 0; i < certificaciondocumentalbarco.verificaciones.length; i++) {
                        var verificacion = certificaciondocumentalbarco.verificaciones[i];

                        verificacion.fechaDesde = new Date(verificacion.fechaDesde);
                        verificacion.fechaHasta = new Date(verificacion.fechaHasta);
                    }

                    $scope.habilitarBotones();

                    getBarcoInfo(certificaciondocumentalbarco.barco);                

                    */
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Find a list of CertificacionDocumentalBarcos
    $scope.find = function () {
      $scope.certificaciondocumentalbarcos = CertificacionDocumentalBarcos.query();
    };
    // Find existing CertificacionDocumentalBarco
    $scope.findOne = function () {
      CertificacionDocumentalBarcos.get({ certificaciondocumentalbarcoId: $stateParams.certificaciondocumentalbarcoId }).$promise.then(function (certificaciondocumentalbarco) {
        $scope.certificaciondocumentalbarco = certificaciondocumentalbarco;
        $scope.certificaciondocumentalbarco.fechaDesde = new Date($scope.certificaciondocumentalbarco.fechaDesde);
        $scope.certificaciondocumentalbarco.fechaHasta = new Date($scope.certificaciondocumentalbarco.fechaHasta);
        for (var i = 0; i < certificaciondocumentalbarco.verificaciones.length; i++) {
          var verificacion = certificaciondocumentalbarco.verificaciones[i];
          verificacion.fechaDesde = new Date(verificacion.fechaDesde);
          verificacion.fechaHasta = new Date(verificacion.fechaHasta);
        }
        $scope.habilitarBotones();
        getBarcoInfo(certificaciondocumentalbarco.barco);
      });
    };
    $scope.nuevaVerificacionComplementaria = function () {
      $scope.certificaciondocumentalbarco.verificaciones.push({
        nombre: 'Verificaci\xf3n Adicional',
        fechaDesde: new Date(),
        fechaHasta: moment(new Date()).add(3, 'month').toDate()
      });
      $scope.habilitarBotones();
    };
    $scope.eliminarVerificacionAnual = function (verificacionAnual) {
      verificacionAnual.deleted = true;
      console.log($scope.certificaciondocumentalbarco.verificaciones);
      $scope.certificaciondocumentalbarco.verificaciones = _.filter($scope.certificaciondocumentalbarco.verificaciones, function (verificacion) {
        return !verificacion.deleted;
      });
    };
    $scope.open = function ($event, control, index) {
      $event.preventDefault();
      $event.stopPropagation();
      console.log(control, index);
      if (index === undefined) {
        $scope.opened[control] = true;
      } else {
        if (!$scope.opened[control]) {
          $scope.opened[control] = {};
        }
        $scope.opened[control][index] = true;
      }
    };
    $scope.canBeDeleted = function () {
      return !_.find($scope.certificaciondocumentalbarco.verificaciones, function (verificacion) {
        return verificacion.cumplida;
      });
    };
    $scope.$on('$destroy', function () {
      Menus.removeMenuItem('topbar', 'grupos/' + $scope.barco.grupo._id);
      Menus.removeMenuItem('topbar', 'barcos/' + $scope.barco._id);
    });
  }
]);'use strict';
//CertificacionDocumentalBarcos service used to communicate CertificacionDocumentalBarcos REST endpoints
angular.module('certificaciondocumentalbarcos').factory('CertificacionDocumentalBarcos', [
  '$resource',
  function ($resource) {
    return $resource('certificaciondocumentalbarcos/:certificaciondocumentalbarcoId', { certificaciondocumentalbarcoId: '@_id' }, { update: { method: 'PUT' } });
  }
]);'use strict';
//Setting up route
angular.module('certificaciondocumentalempresas').config([
  '$stateProvider',
  function ($stateProvider) {
    // CertificacionDocumentalEmpresas state routing
    $stateProvider.state('listCertificacionDocumentalEmpresas', {
      url: '/certificaciondocumentalempresas',
      templateUrl: 'modules/certificaciondocumentalempresas/views/list-certificaciondocumentalempresas.client.view.html'
    }).state('createCertificacionDocumentalEmpresa', {
      url: '/certificaciondocumentalempresas/create/:empresaId',
      templateUrl: 'modules/certificaciondocumentalempresas/views/create-certificaciondocumentalempresa.client.view.html'
    }).state('viewCertificacionDocumentalEmpresa', {
      url: '/certificaciondocumentalempresas/:certificaciondocumentalempresaId',
      templateUrl: 'modules/certificaciondocumentalempresas/views/view-certificaciondocumentalempresa.client.view.html'
    }).state('editCertificacionDocumentalEmpresa', {
      url: '/certificaciondocumentalempresas/:certificaciondocumentalempresaId/edit',
      templateUrl: 'modules/certificaciondocumentalempresas/views/edit-certificaciondocumentalempresa.client.view.html'
    });
  }
]);'use strict';
// CertificacionDocumentalEmpresas controller
angular.module('certificaciondocumentalempresas').controller('CertificacionDocumentalEmpresasController', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'CertificacionDocumentalEmpresas',
  'Grupos',
  'Empresas',
  '$filter',
  'Menus',
  'moment',
  'lodash',
  function ($scope, $stateParams, $location, Authentication, CertificacionDocumentalEmpresas, Grupos, Empresas, $filter, Menus, moment, lodash) {
    $scope.authentication = Authentication;
    $scope.opened = {};
    $scope.empresa = {};
    $scope.certificaciondocumentalempresa = {};
    $scope.isMenuGenerated = false;
    $scope.isNew = $stateParams.empresaId;
    var _ = lodash;
    var getEmpresaInfo = function (empresaId) {
      $scope.empresa = Empresas.get({ empresaId: empresaId }).$promise.then(function (empresa) {
        $scope.empresa = empresa;
        if (!$scope.isMenuGenerated) {
          Menus.addMenuItem('topbar', 'Grupo', 'grupos/' + empresa.grupo._id);
          Menus.addMenuItem('topbar', 'Empresa', 'empresas/' + $scope.empresa._id);
          $scope.isMenuGenerated = true;
        }
      });
    };
    $scope.generarFechas = function () {
      var fechaDesde = $scope.certificaciondocumentalempresa.fechaDesde;
      console.log('generarFechas', fechaDesde);
      $scope.certificaciondocumentalempresa.fechaHasta = moment(fechaDesde).add(5, 'year').toDate();
      for (var i = 0; i <= $scope.certificaciondocumentalempresa.verificaciones.length - 1; i++) {
        var verificacion = $scope.certificaciondocumentalempresa.verificaciones[i];
        verificacion.fechaDesde = moment(fechaDesde).add(i + 1, 'year').add(-3, 'month').toDate();
        verificacion.fechaHasta = moment(fechaDesde).add(i + 1, 'year').add(+3, 'month').toDate();
      }
    };
    $scope.habilitarBotones = function () {
      function habilitarCumplidaFunction(verificacion) {
        return !_.find($scope.certificaciondocumentalempresa.verificaciones, function (otraVerificacion) {
          return otraVerificacion.anioVerificacion < verificacion.anioVerificacion && !otraVerificacion.cumplida;
        });
      }
      $scope.certificaciondocumentalempresa.habilitarCumplida = !_.find($scope.certificaciondocumentalempresa.verificaciones, function (verificacion) {
        return !verificacion.cumplida;
      });
      if (!$scope.certificaciondocumentalempresa.habilitarCumplida) {
        $scope.certificaciondocumentalempresa.cumplida = false;
      }
      for (var i = 0; i < $scope.certificaciondocumentalempresa.verificaciones.length; i++) {
        var verificacion = $scope.certificaciondocumentalempresa.verificaciones[i];
        verificacion.habilitarCumplida = habilitarCumplidaFunction(verificacion);
        if (!verificacion.habilitarCumplida) {
          verificacion.cumplida = false;
        }
      }
    };
    if ($stateParams.empresaId) {
      getEmpresaInfo($stateParams.empresaId);
      $scope.certificaciondocumentalempresa.verificaciones = [];
      for (var i = 1; i <= 4; i++) {
        $scope.certificaciondocumentalempresa.verificaciones.push({
          nombre: 'Verificaci\xf3n Anual ' + i,
          anioVerificacion: i
        });
      }
      $scope.certificaciondocumentalempresa.fechaDesde = new Date();
      $scope.generarFechas();
      $scope.habilitarBotones();
    }
    // Create new CertificacionDocumentalEmpresa
    $scope.create = function () {
      // Create new CertificacionDocumentalEmpresa object
      var certificaciondocumentalempresa = new CertificacionDocumentalEmpresas({
          empresa: $stateParams.empresaId,
          fechaDesde: $scope.certificaciondocumentalempresa.fechaDesde,
          fechaHasta: $scope.certificaciondocumentalempresa.fechaHasta,
          cumplida: $scope.certificaciondocumentalempresa.cumplida,
          comentarios: $scope.certificaciondocumentalempresa.comentarios,
          verificaciones: $scope.certificaciondocumentalempresa.verificaciones
        });
      // Redirect after save
      certificaciondocumentalempresa.$save(function (response) {
        $location.path('empresas/' + $scope.empresa._id);
        // Clear form fields
        $scope.fechaDesde = null;
        $scope.fechaHasta = null;
        $scope.cumplida = false;
        $scope.comentarios = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Remove existing CertificacionDocumentalEmpresa
    $scope.remove = function (certificaciondocumentalempresa) {
      $scope.certificaciondocumentalempresa.$remove(function () {
        $location.path('empresas/' + $scope.empresa._id);
      });
    };
    // Update existing CertificacionDocumentalEmpresa
    $scope.update = function () {
      var certificaciondocumentalempresa = $scope.certificaciondocumentalempresa;
      certificaciondocumentalempresa.$update(function () {
        $location.path('empresas/' + $scope.empresa._id);  /*
                    $scope.certificaciondocumentalempresa.fechaDesde = new Date($scope.certificaciondocumentalempresa.fechaDesde);
                    $scope.certificaciondocumentalempresa.fechaHasta = new Date($scope.certificaciondocumentalempresa.fechaHasta);

                    for (var i = 0; i < certificaciondocumentalempresa.verificaciones.length; i++) {
                        var verificacion = certificaciondocumentalempresa.verificaciones[i];

                        verificacion.fechaDesde = new Date(verificacion.fechaDesde);
                        verificacion.fechaHasta = new Date(verificacion.fechaHasta);
                    }

                    $scope.habilitarBotones();

                    getEmpresaInfo(certificaciondocumentalempresa.empresa);                

                    */
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Find a list of CertificacionDocumentalEmpresas
    $scope.find = function () {
      $scope.certificaciondocumentalempresas = CertificacionDocumentalEmpresas.query();
    };
    // Find existing CertificacionDocumentalEmpresa
    $scope.findOne = function () {
      CertificacionDocumentalEmpresas.get({ certificaciondocumentalempresaId: $stateParams.certificaciondocumentalempresaId }).$promise.then(function (certificaciondocumentalempresa) {
        $scope.certificaciondocumentalempresa = certificaciondocumentalempresa;
        $scope.certificaciondocumentalempresa.fechaDesde = new Date($scope.certificaciondocumentalempresa.fechaDesde);
        $scope.certificaciondocumentalempresa.fechaHasta = new Date($scope.certificaciondocumentalempresa.fechaHasta);
        for (var i = 0; i < certificaciondocumentalempresa.verificaciones.length; i++) {
          var verificacion = certificaciondocumentalempresa.verificaciones[i];
          verificacion.fechaDesde = new Date(verificacion.fechaDesde);
          verificacion.fechaHasta = new Date(verificacion.fechaHasta);
        }
        $scope.habilitarBotones();
        getEmpresaInfo(certificaciondocumentalempresa.empresa);
      });
    };
    $scope.nuevaVerificacionComplementaria = function () {
      $scope.certificaciondocumentalempresa.verificaciones.push({
        nombre: 'Verificaci\xf3n Adicional',
        fechaDesde: new Date(),
        fechaHasta: moment(new Date()).add(3, 'month').toDate()
      });
      $scope.habilitarBotones();
    };
    $scope.eliminarVerificacionAnual = function (verificacionAnual) {
      verificacionAnual.deleted = true;
      console.log($scope.certificaciondocumentalempresa.verificaciones);
      $scope.certificaciondocumentalempresa.verificaciones = _.filter($scope.certificaciondocumentalempresa.verificaciones, function (verificacion) {
        return !verificacion.deleted;
      });
    };
    $scope.open = function ($event, control, index) {
      $event.preventDefault();
      $event.stopPropagation();
      console.log(control, index);
      if (index === undefined) {
        $scope.opened[control] = true;
      } else {
        if (!$scope.opened[control]) {
          $scope.opened[control] = {};
        }
        $scope.opened[control][index] = true;
      }
    };
    $scope.canBeDeleted = function () {
      return !_.find($scope.certificaciondocumentalempresa.verificaciones, function (verificacion) {
        return verificacion.cumplida;
      });
    };
    $scope.$on('$destroy', function () {
      Menus.removeMenuItem('topbar', 'grupos/' + $scope.empresa.grupo._id);
      Menus.removeMenuItem('topbar', 'empresas/' + $scope.empresa._id);
    });
  }
]);'use strict';
//CertificacionDocumentalEmpresas service used to communicate CertificacionDocumentalEmpresas REST endpoints
angular.module('certificaciondocumentalempresas').factory('CertificacionDocumentalEmpresas', [
  '$resource',
  function ($resource) {
    return $resource('certificaciondocumentalempresas/:certificaciondocumentalempresaId', { certificaciondocumentalempresaId: '@_id' }, { update: { method: 'PUT' } });
  }
]);'use strict';
//Setting up route
angular.module('certificaciontecnicabarcos').config([
  '$stateProvider',
  function ($stateProvider) {
    // CertificacionTecnicaBarcos state routing
    $stateProvider.state('listCertificacionTecnicaBarcos', {
      url: '/certificaciontecnicabarcos',
      templateUrl: 'modules/certificaciontecnicabarcos/views/list-certificaciontecnicabarcos.client.view.html'
    }).state('createCertificacionTecnicaBarco', {
      url: '/certificaciontecnicabarcos/create/:barcoId',
      templateUrl: 'modules/certificaciontecnicabarcos/views/create-certificaciontecnicabarco.client.view.html'
    }).state('viewCertificacionTecnicaBarco', {
      url: '/certificaciontecnicabarcos/:certificaciontecnicabarcoId',
      templateUrl: 'modules/certificaciontecnicabarcos/views/view-certificaciontecnicabarco.client.view.html'
    }).state('editCertificacionTecnicaBarco', {
      url: '/certificaciontecnicabarcos/:certificaciontecnicabarcoId/edit',
      templateUrl: 'modules/certificaciontecnicabarcos/views/edit-certificaciontecnicabarco.client.view.html'
    });
  }
]);'use strict';
// CertificacionTecnicaBarcos controller
angular.module('certificaciontecnicabarcos').controller('CertificacionTecnicaBarcosController', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'CertificacionTecnicaBarcos',
  'Tipocertificaciontecnicabarcos',
  'Grupos',
  'Barcos',
  '$filter',
  'Menus',
  'moment',
  'lodash',
  function ($scope, $stateParams, $location, Authentication, CertificacionTecnicaBarcos, Tipocertificaciontecnicabarcos, Grupos, Barcos, $filter, Menus, moment, lodash) {
    $scope.authentication = Authentication;
    $scope.opened = {};
    $scope.barco = {};
    $scope.certificaciontecnicabarco = {};
    $scope.isMenuGenerated = false;
    $scope.isNew = $stateParams.barcoId;
    var verificacionesConfig = [
        {
          nombre: 'Verificaci\xf3n Intermedia Casco en Seco',
          sumarMeses: 36,
          dispersionMeses: 6,
          tipoVerifiaccion: 1,
          anioVerificacion: 1
        },
        {
          nombre: 'Verificaci\xf3n Intermedia de M\xe1quinas',
          sumarMeses: 36,
          dispersionMeses: 6,
          tipoVerifiaccion: 2,
          anioVerificacion: 1
        },
        {
          nombre: 'Verificaci\xf3n Intermedia de Electricidad',
          sumarMeses: 36,
          dispersionMeses: 6,
          tipoVerifiaccion: 3,
          anioVerificacion: 1
        },
        {
          nombre: '1\xb0 Verificaci\xf3n Complementaria de Armamento',
          sumarMeses: 18,
          dispersionMeses: 3,
          tipoVerifiaccion: 4,
          anioVerificacion: 1
        },
        {
          nombre: '1\xb0 Verificaci\xf3n Complementaria de Radio',
          sumarMeses: 18,
          dispersionMeses: 3,
          tipoVerifiaccion: 4,
          anioVerificacion: 2
        },
        {
          nombre: '2\xb0 Verificaci\xf3n Complementaria de Armamento',
          sumarMeses: 54,
          dispersionMeses: 3,
          tipoVerifiaccion: 5,
          anioVerificacion: 1
        },
        {
          nombre: '2\xb0 Verificaci\xf3n Complementaria de Radio',
          sumarMeses: 54,
          dispersionMeses: 3,
          tipoVerifiaccion: 5,
          anioVerificacion: 2
        }
      ];
    var _ = lodash;
    var getBarcoInfo = function (barcoId) {
      $scope.barco = Barcos.get({ barcoId: barcoId }).$promise.then(function (barco) {
        $scope.barco = barco;
        if (!$scope.isMenuGenerated) {
          Menus.addMenuItem('topbar', 'Grupo', 'grupos/' + barco.grupo._id);
          Menus.addMenuItem('topbar', 'Barco', 'barcos/' + $scope.barco._id);
          $scope.isMenuGenerated = true;
        }
      });
      $scope.tipoCertificacionTecnicaBarcosList = Tipocertificaciontecnicabarcos.query();
    };
    $scope.generarFechas = function () {
      var fechaDesde = $scope.certificaciontecnicabarco.fechaDesde;
      console.log('generarFechas', fechaDesde);
      $scope.certificaciontecnicabarco.fechaHasta = moment(fechaDesde).add(6, 'year').toDate();
      for (var i = 0; i <= verificacionesConfig.length - 1; i++) {
        var verificacionConfig = verificacionesConfig[i];
        var verificacion = $scope.certificaciontecnicabarco.verificaciones[i];
        verificacion.fechaDesde = moment(fechaDesde).add(verificacionConfig.sumarMeses - verificacionConfig.dispersionMeses, 'month').toDate();
        verificacion.fechaHasta = moment(fechaDesde).add(verificacionConfig.sumarMeses + verificacionConfig.dispersionMeses, 'month').toDate();
      }
    };
    $scope.habilitarBotones = function () {
      function habilitarCumplidaFunction(verificacion) {
        return !_.find($scope.certificaciontecnicabarco.verificaciones, function (otraVerificacion) {
          return otraVerificacion.tipoVerifiaccion === verificacion.tipoVerifiaccion && otraVerificacion.anioVerificacion < verificacion.anioVerificacion && !otraVerificacion.cumplida;
        });
      }
      $scope.certificaciontecnicabarco.habilitarCumplida = !_.find($scope.certificaciontecnicabarco.verificaciones, function (verificacion) {
        return !verificacion.cumplida;
      });
      if (!$scope.certificaciontecnicabarco.habilitarCumplida) {
        $scope.certificaciontecnicabarco.cumplida = false;
      }
      for (var i = 0; i < $scope.certificaciontecnicabarco.verificaciones.length; i++) {
        var verificacion = $scope.certificaciontecnicabarco.verificaciones[i];
        verificacion.habilitarCumplida = habilitarCumplidaFunction(verificacion);
        if (!verificacion.habilitarCumplida) {
          verificacion.cumplida = false;
        }
      }
    };
    if ($stateParams.barcoId) {
      getBarcoInfo($stateParams.barcoId);
      $scope.certificaciontecnicabarco.verificaciones = [];
      $scope.certificaciontecnicabarco.fechaDesde = new Date();
      for (var i = 0; i < verificacionesConfig.length; i++) {
        var verificacionConfig = verificacionesConfig[i];
        $scope.certificaciontecnicabarco.verificaciones.push({
          nombre: verificacionConfig.nombre,
          tipoVerifiaccion: verificacionConfig.tipoVerifiaccion,
          anioVerificacion: verificacionConfig.anioVerificacion
        });
      }
      $scope.generarFechas();
      $scope.habilitarBotones();
    }
    // Create new CertificacionTecnicaBarco
    $scope.create = function () {
      // Create new CertificacionTecnicaBarco object
      var certificaciontecnicabarco = new CertificacionTecnicaBarcos({
          barco: $stateParams.barcoId,
          fechaDesde: $scope.certificaciontecnicabarco.fechaDesde,
          fechaHasta: $scope.certificaciontecnicabarco.fechaHasta,
          cumplida: $scope.certificaciontecnicabarco.cumplida,
          comentarios: $scope.certificaciontecnicabarco.comentarios,
          verificaciones: $scope.certificaciontecnicabarco.verificaciones
        });
      // Redirect after save
      certificaciontecnicabarco.$save(function (response) {
        $location.path('barcos/' + $scope.barco._id);
        // Clear form fields
        $scope.fechaDesde = null;
        $scope.fechaHasta = null;
        $scope.cumplida = false;
        $scope.comentarios = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Remove existing CertificacionTecnicaBarco
    $scope.remove = function (certificaciontecnicabarco) {
      if (certificaciontecnicabarco) {
        certificaciontecnicabarco.$remove();
        for (var i in $scope.certificaciontecnicabarcos) {
          if ($scope.certificaciontecnicabarcos[i] === certificaciontecnicabarco) {
            $scope.certificaciontecnicabarcos.splice(i, 1);
          }
        }
      } else {
        $scope.certificaciontecnicabarco.$remove(function () {
          $location.path('barcos/' + $scope.barco._id);
        });
      }
    };
    // Update existing CertificacionTecnicaBarco
    $scope.update = function () {
      var certificaciontecnicabarco = $scope.certificaciontecnicabarco;
      certificaciontecnicabarco.$update(function () {
        $location.path('barcos/' + $scope.barco._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Find a list of CertificacionTecnicaBarcos
    $scope.find = function () {
      $scope.certificaciontecnicabarcos = CertificacionTecnicaBarcos.query();
    };
    // Find existing CertificacionTecnicaBarco
    $scope.findOne = function () {
      CertificacionTecnicaBarcos.get({ certificaciontecnicabarcoId: $stateParams.certificaciontecnicabarcoId }).$promise.then(function (certificaciontecnicabarco) {
        $scope.certificaciontecnicabarco = certificaciontecnicabarco;
        $scope.certificaciontecnicabarco.fechaDesde = new Date($scope.certificaciontecnicabarco.fechaDesde);
        $scope.certificaciontecnicabarco.fechaHasta = new Date($scope.certificaciontecnicabarco.fechaHasta);
        for (var i = 0; i < certificaciontecnicabarco.verificaciones.length; i++) {
          var verificacion = certificaciontecnicabarco.verificaciones[i];
          verificacion.fechaDesde = new Date(verificacion.fechaDesde);
          verificacion.fechaHasta = new Date(verificacion.fechaHasta);
        }
        $scope.habilitarBotones();
        getBarcoInfo(certificaciontecnicabarco.barco);
      });
    };
    $scope.nuevaVerificacionComplementaria = function () {
      $scope.certificaciontecnicabarco.verificaciones.push({
        nombre: 'Verificaci\xf3n Adicional',
        fechaDesde: new Date(),
        fechaHasta: moment(new Date()).add(3, 'month').toDate()
      });
      $scope.habilitarBotones();
    };
    $scope.eliminarVerificacionAnual = function (verificacionAnual) {
      verificacionAnual.deleted = true;
      console.log($scope.certificaciontecnicabarco.verificaciones);
      $scope.certificaciontecnicabarco.verificaciones = _.filter($scope.certificaciontecnicabarco.verificaciones, function (verificacion) {
        return !verificacion.deleted;
      });
    };
    $scope.open = function ($event, control, index) {
      $event.preventDefault();
      $event.stopPropagation();
      console.log(control, index);
      if (index === undefined) {
        $scope.opened[control] = true;
      } else {
        if (!$scope.opened[control]) {
          $scope.opened[control] = {};
        }
        $scope.opened[control][index] = true;
      }
    };
    $scope.canBeDeleted = function () {
      return !_.find($scope.certificaciontecnicabarco.verificaciones, function (verificacion) {
        return verificacion.cumplida;
      });
    };
    $scope.$on('$destroy', function () {
      Menus.removeMenuItem('topbar', 'grupos/' + $scope.barco.grupo._id);
      Menus.removeMenuItem('topbar', 'barcos/' + $scope.barco._id);
    });
  }
]);'use strict';
//CertificacionTecnicaBarcos service used to communicate CertificacionTecnicaBarcos REST endpoints
angular.module('certificaciontecnicabarcos').factory('CertificacionTecnicaBarcos', [
  '$resource',
  function ($resource) {
    return $resource('certificaciontecnicabarcos/:certificaciontecnicabarcoId', { certificaciontecnicabarcoId: '@_id' }, { update: { method: 'PUT' } });
  }
]);'use strict';
// Setting up route
angular.module('core').config([
  '$stateProvider',
  '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {
    // Redirect to home view when route not found
    $urlRouterProvider.otherwise('/');
    // Home state routing
    $stateProvider.state('home', {
      url: '/',
      templateUrl: 'modules/core/views/home.client.view.html'
    });
  }
]);'use strict';
angular.module('core').controller('HeaderController', [
  '$scope',
  'Authentication',
  'Menus',
  function ($scope, Authentication, Menus) {
    $scope.authentication = Authentication;
    $scope.isCollapsed = false;
    $scope.menu = Menus.getMenu('topbar');
    $scope.toggleCollapsibleMenu = function () {
      $scope.isCollapsed = !$scope.isCollapsed;
    };
    // Collapsing the menu after navigation
    $scope.$on('$stateChangeSuccess', function () {
      $scope.isCollapsed = false;
    });
  }
]);'use strict';
angular.module('core').controller('HomeController', [
  '$scope',
  'Authentication',
  '$location',
  function ($scope, Authentication, $location) {
    // This provides Authentication context.
    $scope.authentication = Authentication;
    $scope.initScreen = function () {
      if ($scope.authentication.user) {
        $location.path('/dashboard');
      } else {
        $location.path('/signin');
      }
    };
  }
]);'use strict';
//Menu service used for managing  menus
angular.module('core').service('Menus', [function () {
    // Define a set of default roles
    this.defaultRoles = ['*'];
    // Define the menus object
    this.menus = {};
    // A private function for rendering decision 
    var shouldRender = function (user) {
      if (user) {
        if (!!~this.roles.indexOf('*')) {
          return true;
        } else {
          for (var userRoleIndex in user.roles) {
            for (var roleIndex in this.roles) {
              if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
                return true;
              }
            }
          }
        }
      } else {
        return this.isPublic;
      }
      return false;
    };
    // Validate menu existance
    this.validateMenuExistance = function (menuId) {
      if (menuId && menuId.length) {
        if (this.menus[menuId]) {
          return true;
        } else {
          throw new Error('Menu does not exists');
        }
      } else {
        throw new Error('MenuId was not provided');
      }
      return false;
    };
    // Get the menu object by menu id
    this.getMenu = function (menuId) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Return the menu object
      return this.menus[menuId];
    };
    // Add new menu object by menu id
    this.addMenu = function (menuId, isPublic, roles) {
      // Create the new menu
      this.menus[menuId] = {
        isPublic: isPublic || false,
        roles: roles || this.defaultRoles,
        items: [],
        shouldRender: shouldRender
      };
      // Return the menu object
      return this.menus[menuId];
    };
    // Remove existing menu object by menu id
    this.removeMenu = function (menuId) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Return the menu object
      delete this.menus[menuId];
    };
    // Add menu item object
    this.addMenuItem = function (menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Push new menu item
      this.menus[menuId].items.push({
        title: menuItemTitle,
        link: menuItemURL,
        menuItemType: menuItemType || 'item',
        menuItemClass: menuItemType,
        uiRoute: menuItemUIRoute || '/' + menuItemURL,
        isPublic: isPublic === null || typeof isPublic === 'undefined' ? this.menus[menuId].isPublic : isPublic,
        roles: roles === null || typeof roles === 'undefined' ? this.menus[menuId].roles : roles,
        position: position || 0,
        items: [],
        shouldRender: shouldRender
      });
      // Return the menu object
      return this.menus[menuId];
    };
    // Add submenu item object
    this.addSubMenuItem = function (menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Search for menu item
      for (var itemIndex in this.menus[menuId].items) {
        if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
          // Push new submenu item
          this.menus[menuId].items[itemIndex].items.push({
            title: menuItemTitle,
            link: menuItemURL,
            uiRoute: menuItemUIRoute || '/' + menuItemURL,
            isPublic: isPublic === null || typeof isPublic === 'undefined' ? this.menus[menuId].items[itemIndex].isPublic : isPublic,
            roles: roles === null || typeof roles === 'undefined' ? this.menus[menuId].items[itemIndex].roles : roles,
            position: position || 0,
            shouldRender: shouldRender
          });
        }
      }
      // Return the menu object
      return this.menus[menuId];
    };
    // Remove existing menu object by menu id
    this.removeMenuItem = function (menuId, menuItemURL) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Search for menu item to remove
      for (var itemIndex in this.menus[menuId].items) {
        if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
          this.menus[menuId].items.splice(itemIndex, 1);
        }
      }
      // Return the menu object
      return this.menus[menuId];
    };
    // Remove existing menu object by menu id
    this.removeSubMenuItem = function (menuId, submenuItemURL) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Search for menu item to remove
      for (var itemIndex in this.menus[menuId].items) {
        for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
          if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
            this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
          }
        }
      }
      // Return the menu object
      return this.menus[menuId];
    };
    //Adding the topbar menu
    this.addMenu('topbar');
  }]);'use strict';
//Setting up route
angular.module('empresas').config([
  '$stateProvider',
  function ($stateProvider) {
    // Empresas state routing
    $stateProvider.state('listEmpresas', {
      url: '/empresas',
      templateUrl: 'modules/empresas/views/list-empresas.client.view.html'
    }).state('createEmpresa', {
      url: '/empresas/create/:grupoId',
      templateUrl: 'modules/empresas/views/create-empresa.client.view.html'
    }).state('viewEmpresa', {
      url: '/empresas/:empresaId',
      templateUrl: 'modules/empresas/views/view-empresa.client.view.html'
    }).state('editEmpresa', {
      url: '/empresas/:empresaId/edit',
      templateUrl: 'modules/empresas/views/edit-empresa.client.view.html'
    });
  }
]);'use strict';
// Empresas controller
angular.module('empresas').controller('EmpresasController', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'Empresas',
  'Grupos',
  'Menus',
  '$modal',
  '$log',
  'lodash',
  function ($scope, $stateParams, $location, Authentication, Empresas, Grupos, Menus, $modal, $log, lodash) {
    $scope.authentication = Authentication;
    var _ = lodash;
    var grupoId = $stateParams.grupoId;
    if (grupoId) {
      Menus.addMenuItem('topbar', 'Grupo', 'grupos/' + grupoId);
    }
    // Create new Empresa
    $scope.create = function () {
      // Create new Empresa object
      var empresa = new Empresas({
          grupo: $stateParams.grupoId,
          nombre: this.nombre
        });
      // Redirect after save
      empresa.$save(function (response) {
        $location.path('empresas/' + response._id);
        // Clear form fields
        $scope.nombre = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Remove existing Empresa
    $scope.remove = function (empresa) {
      var modalInstance = $modal.open({
          templateUrl: '/modules/grupos/views/delete-modal.html',
          size: 'sm',
          resolve: {
            items: function () {
              return $scope.items;
            }
          }
        });
      modalInstance.result.then(function () {
        if (empresa) {
          empresa.$remove();
          for (var i in $scope.empresas) {
            if ($scope.empresas[i] === empresa) {
              $scope.empresas.splice(i, 1);
            }
          }
        } else {
          $scope.empresa.$remove(function () {
            $location.path('empresas');
          });
        }
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };
    // Update existing Empresa
    $scope.update = function () {
      var empresa = $scope.empresa;
      empresa.$update(function () {
        $location.path('empresas/' + empresa._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Find a list of Empresas
    $scope.find = function () {
      $scope.empresas = Empresas.query();
    };
    // Find existing Empresa
    $scope.findOne = function () {
      Empresas.get({ empresaId: $stateParams.empresaId }).$promise.then(function (result) {
        $scope.empresa = result;
        grupoId = result.grupo._id;
        Menus.addMenuItem('topbar', 'Grupo', 'grupos/' + grupoId);
      });
    };
    $scope.hasCertificacionPorCumplir = function () {
      return $scope.empresa && _.findWhere($scope.empresa.certificacionesDocumentales, { cumplida: false });
    };
    $scope.$on('$destroy', function () {
      Menus.removeMenuItem('topbar', 'grupos/' + grupoId);
    });
  }
]);'use strict';
//Empresas service used to communicate Empresas REST endpoints
angular.module('empresas').factory('Empresas', [
  '$resource',
  function ($resource) {
    return $resource('empresas/:empresaId', { empresaId: '@_id' }, { update: { method: 'PUT' } });
  }
]);'use strict';
// Configuring the Articles module
angular.module('grupos').run([
  'Menus',
  function (Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', 'Administraci\xf3n', 'grupos');
    Menus.addMenuItem('topbar', 'Tareas', 'dashboard');
  }
]);'use strict';
//Setting up route
angular.module('grupos').config([
  '$stateProvider',
  function ($stateProvider) {
    // Grupos state routing
    $stateProvider.state('listGrupos', {
      url: '/grupos',
      templateUrl: 'modules/grupos/views/list-grupos.client.view.html'
    }).state('createGrupo', {
      url: '/grupos/create',
      templateUrl: 'modules/grupos/views/create-grupo.client.view.html'
    }).state('viewGrupo', {
      url: '/grupos/:grupoId',
      templateUrl: 'modules/grupos/views/view-grupo.client.view.html'
    }).state('editGrupo', {
      url: '/grupos/:grupoId/edit',
      templateUrl: 'modules/grupos/views/edit-grupo.client.view.html'
    }).state('dashboard', {
      url: '/dashboard',
      templateUrl: 'modules/grupos/views/dashboard.html'
    });
  }
]);'use strict';
// Grupos controller
angular.module('grupos').controller('GruposController', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'Grupos',
  'Menus',
  '$http',
  '$modal',
  '$log',
  'Pagination',
  '$filter',
  function ($scope, $stateParams, $location, Authentication, Grupos, Menus, $http, $modal, $log, Pagination, $filter) {
    $scope.authentication = Authentication;
    console.log(Authentication);
    $scope.pagination = Pagination.getNew();
    $scope.pagination = Pagination.getNew(5);
    // Create new Grupo
    $scope.create = function () {
      // Create new Grupo object
      var grupo = new Grupos({
          nombre: this.nombre,
          descripcion: this.descripcion
        });
      // Redirect after save
      grupo.$save(function (response) {
        $location.path('grupos/' + response._id);
        // Clear form fields
        $scope.nombre = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Remove existing Grupo
    $scope.remove = function (grupo) {
      var modalInstance = $modal.open({
          templateUrl: '/modules/grupos/views/delete-modal.html',
          size: 'sm',
          resolve: {
            items: function () {
              return $scope.items;
            }
          }
        });
      modalInstance.result.then(function () {
        if (grupo) {
          grupo.$remove();
          for (var i in $scope.grupos) {
            if ($scope.grupos[i] === grupo) {
              $scope.grupos.splice(i, 1);
            }
          }
        } else {
          $scope.grupo.$remove(function () {
            $location.path('grupos');
          });
        }
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };
    // Update existing Grupo
    $scope.update = function () {
      var grupo = $scope.grupo;
      grupo.$update(function () {
        $location.path('grupos/' + grupo._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Find a list of Grupos
    $scope.find = function () {
      $scope.grupos = Grupos.query();
    };
    // Find existing Grupo
    $scope.findOne = function () {
      $scope.grupo = Grupos.get({ grupoId: $stateParams.grupoId });
    };
    $scope.getDashboardInfo = function () {
      var request = $http({
          method: 'get',
          url: '/dashboard'
        }).success(function (data) {
          $scope.dashboardData = data;
          $scope.pagination.numPages = Math.ceil(data.length / $scope.pagination.perPage);
        }).error(function (error) {
          console.log(error);
        });
    };
    $scope.$watch('searchText', function (searchText) {
      var data = $filter('multiWordFilter')($scope.dashboardData, searchText);
      if (data) {
        $scope.pagination.numPages = Math.ceil(data.length / $scope.pagination.perPage);
      }
    });
  }
]);'use strict';
angular.module('grupos').directive('auditInfo', [function () {
    return {
      templateUrl: '/modules/grupos/views/audit-info.client.directive.html',
      restrict: 'E',
      scope: { entity: '=entity' },
      link: function postLink(scope, element, attrs) {
      }
    };
  }]);'use strict';
angular.module('grupos').filter('multiWordFilter', [
  '$filter',
  function ($filter) {
    return function (inputArray, searchText) {
      var wordArray = searchText ? searchText.toLowerCase().split(/\s+/) : [];
      var wordCount = wordArray.length;
      for (var i = 0; i < wordCount; i++) {
        inputArray = $filter('filter')(inputArray, wordArray[i]);
      }
      return inputArray;
    };
  }
]);'use strict';
//Grupos service used to communicate Grupos REST endpoints
angular.module('grupos').factory('Grupos', [
  '$resource',
  function ($resource) {
    return $resource('grupos/:grupoId', { grupoId: '@_id' }, { update: { method: 'PUT' } });
  }
]);'use strict';
//Setting up route
angular.module('tipocertificaciontecnicabarcos').config([
  '$stateProvider',
  function ($stateProvider) {
    // Tipocertificaciontecnicabarcos state routing
    $stateProvider.state('listTipocertificaciontecnicabarcos', {
      url: '/tipocertificaciontecnicabarcos',
      templateUrl: 'modules/tipocertificaciontecnicabarcos/views/list-tipocertificaciontecnicabarcos.client.view.html'
    }).state('createTipocertificaciontecnicabarco', {
      url: '/tipocertificaciontecnicabarcos/create',
      templateUrl: 'modules/tipocertificaciontecnicabarcos/views/create-tipocertificaciontecnicabarco.client.view.html'
    }).state('viewTipocertificaciontecnicabarco', {
      url: '/tipocertificaciontecnicabarcos/:tipocertificaciontecnicabarcoId',
      templateUrl: 'modules/tipocertificaciontecnicabarcos/views/view-tipocertificaciontecnicabarco.client.view.html'
    }).state('editTipocertificaciontecnicabarco', {
      url: '/tipocertificaciontecnicabarcos/:tipocertificaciontecnicabarcoId/edit',
      templateUrl: 'modules/tipocertificaciontecnicabarcos/views/edit-tipocertificaciontecnicabarco.client.view.html'
    });
  }
]);'use strict';
// Tipocertificaciontecnicabarcos controller
angular.module('tipocertificaciontecnicabarcos').controller('TipocertificaciontecnicabarcosController', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'Tipocertificaciontecnicabarcos',
  function ($scope, $stateParams, $location, Authentication, Tipocertificaciontecnicabarcos) {
    $scope.authentication = Authentication;
    // Create new Tipocertificaciontecnicabarco
    $scope.create = function () {
      // Create new Tipocertificaciontecnicabarco object
      var tipocertificaciontecnicabarco = new Tipocertificaciontecnicabarcos({ nombre: this.nombre });
      // Redirect after save
      tipocertificaciontecnicabarco.$save(function (response) {
        $location.path('tipocertificaciontecnicabarcos/' + response._id);
        // Clear form fields
        $scope.nombre = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Remove existing Tipocertificaciontecnicabarco
    $scope.remove = function (tipocertificaciontecnicabarco) {
      if (tipocertificaciontecnicabarco) {
        tipocertificaciontecnicabarco.$remove();
        for (var i in $scope.tipocertificaciontecnicabarcos) {
          if ($scope.tipocertificaciontecnicabarcos[i] === tipocertificaciontecnicabarco) {
            $scope.tipocertificaciontecnicabarcos.splice(i, 1);
          }
        }
      } else {
        $scope.tipocertificaciontecnicabarco.$remove(function () {
          $location.path('tipocertificaciontecnicabarcos');
        });
      }
    };
    // Update existing Tipocertificaciontecnicabarco
    $scope.update = function () {
      var tipocertificaciontecnicabarco = $scope.tipocertificaciontecnicabarco;
      tipocertificaciontecnicabarco.$update(function () {
        $location.path('tipocertificaciontecnicabarcos/' + tipocertificaciontecnicabarco._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Find a list of Tipocertificaciontecnicabarcos
    $scope.find = function () {
      $scope.tipocertificaciontecnicabarcos = Tipocertificaciontecnicabarcos.query();
    };
    // Find existing Tipocertificaciontecnicabarco
    $scope.findOne = function () {
      $scope.tipocertificaciontecnicabarco = Tipocertificaciontecnicabarcos.get({ tipocertificaciontecnicabarcoId: $stateParams.tipocertificaciontecnicabarcoId });
    };
  }
]);'use strict';
//Tipocertificaciontecnicabarcos service used to communicate Tipocertificaciontecnicabarcos REST endpoints
angular.module('tipocertificaciontecnicabarcos').factory('Tipocertificaciontecnicabarcos', [
  '$resource',
  function ($resource) {
    return $resource('tipocertificaciontecnicabarcos/:tipocertificaciontecnicabarcoId', { tipocertificaciontecnicabarcoId: '@_id' }, { update: { method: 'PUT' } });
  }
]);'use strict';
// Config HTTP Error Handling
angular.module('users').config([
  '$httpProvider',
  function ($httpProvider) {
    // Set the httpProvider "not authorized" interceptor
    $httpProvider.interceptors.push([
      '$q',
      '$location',
      'Authentication',
      function ($q, $location, Authentication) {
        return {
          responseError: function (rejection) {
            switch (rejection.status) {
            case 401:
              // Deauthenticate the global user
              Authentication.user = null;
              // Redirect to signin page
              $location.path('signin');
              break;
            case 403:
              // Add unauthorized behaviour 
              break;
            }
            return $q.reject(rejection);
          }
        };
      }
    ]);
  }
]);'use strict';
// Setting up route
angular.module('users').config([
  '$stateProvider',
  function ($stateProvider) {
    // Users state routing
    $stateProvider.state('profile', {
      url: '/settings/profile',
      templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
    }).state('password', {
      url: '/settings/password',
      templateUrl: 'modules/users/views/settings/change-password.client.view.html'
    }).state('accounts', {
      url: '/settings/accounts',
      templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
    }).state('signup', {
      url: '/signup',
      templateUrl: 'modules/users/views/authentication/signup.client.view.html'
    }).state('signin', {
      url: '/signin',
      templateUrl: 'modules/users/views/authentication/signin.client.view.html'
    }).state('forgot', {
      url: '/password/forgot',
      templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
    }).state('reset-invlaid', {
      url: '/password/reset/invalid',
      templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
    }).state('reset-success', {
      url: '/password/reset/success',
      templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
    }).state('reset', {
      url: '/password/reset/:token',
      templateUrl: 'modules/users/views/password/reset-password.client.view.html'
    });
  }
]);'use strict';
angular.module('users').controller('AuthenticationController', [
  '$scope',
  '$http',
  '$location',
  'Authentication',
  function ($scope, $http, $location, Authentication) {
    $scope.authentication = Authentication;
    $scope.signup = function () {
      // If user is not signed in then redirect back home
      if (!$scope.authentication.user) {
        $location.path('/');
      } else {
        $http.post('/auth/signup', $scope.credentials).success(function (response) {
          // If successful we assign the response to the global user model
          $scope.authentication.user = response;
          // And redirect to the index page
          $location.path('/');
        }).error(function (response) {
          $scope.error = response.message;
        });
      }
    };
    $scope.signin = function () {
      // If user is signed in then redirect back home
      if ($scope.authentication.user) {
        $location.path('/');
      } else {
        $http.post('/auth/signin', $scope.credentials).success(function (response) {
          // If successful we assign the response to the global user model
          $scope.authentication.user = response;
          // And redirect to the index page
          $location.path('/');
        }).error(function (response) {
          $scope.error = response.message;
        });
      }
    };
  }
]);'use strict';
angular.module('users').controller('PasswordController', [
  '$scope',
  '$stateParams',
  '$http',
  '$location',
  'Authentication',
  function ($scope, $stateParams, $http, $location, Authentication) {
    $scope.authentication = Authentication;
    //If user is signed in then redirect back home
    if ($scope.authentication.user)
      $location.path('/');
    // Submit forgotten password account id
    $scope.askForPasswordReset = function () {
      $scope.success = $scope.error = null;
      $http.post('/auth/forgot', $scope.credentials).success(function (response) {
        // Show user success message and clear form
        $scope.credentials = null;
        $scope.success = response.message;
      }).error(function (response) {
        // Show user error message and clear form
        $scope.credentials = null;
        $scope.error = response.message;
      });
    };
    // Change user password
    $scope.resetUserPassword = function () {
      $scope.success = $scope.error = null;
      $http.post('/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.passwordDetails = null;
        // Attach user profile
        Authentication.user = response;
        // And redirect to the index page
        $location.path('/password/reset/success');
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);'use strict';
angular.module('users').controller('SettingsController', [
  '$scope',
  '$http',
  '$location',
  'Users',
  'Authentication',
  function ($scope, $http, $location, Users, Authentication) {
    $scope.user = Authentication.user;
    // If user is not signed in then redirect back home
    if (!$scope.user)
      $location.path('/');
    // Check if there are additional accounts 
    $scope.hasConnectedAdditionalSocialAccounts = function (provider) {
      for (var i in $scope.user.additionalProvidersData) {
        return true;
      }
      return false;
    };
    // Check if provider is already in use with current user
    $scope.isConnectedSocialAccount = function (provider) {
      return $scope.user.provider === provider || $scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider];
    };
    // Remove a user social account
    $scope.removeUserSocialAccount = function (provider) {
      $scope.success = $scope.error = null;
      $http.delete('/users/accounts', { params: { provider: provider } }).success(function (response) {
        // If successful show success message and clear form
        $scope.success = true;
        $scope.user = Authentication.user = response;
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
    // Update a user profile
    $scope.updateUserProfile = function (isValid) {
      if (isValid) {
        $scope.success = $scope.error = null;
        var user = new Users($scope.user);
        user.$update(function (response) {
          $scope.success = true;
          Authentication.user = response;
        }, function (response) {
          $scope.error = response.data.message;
        });
      } else {
        $scope.submitted = true;
      }
    };
    // Change user password
    $scope.changeUserPassword = function () {
      $scope.success = $scope.error = null;
      $http.post('/users/password', $scope.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.success = true;
        $scope.passwordDetails = null;
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);'use strict';
// Authentication service for user variables
angular.module('users').factory('Authentication', [function () {
    var _this = this;
    _this._data = { user: window.user };
    return _this._data;
  }]);'use strict';
// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', [
  '$resource',
  function ($resource) {
    return $resource('users', {}, { update: { method: 'PUT' } });
  }
]);